 /**
 * Payment Manager Orders List Item
 */
EmberApp.PmAuthorizeListItemComponent = Ember.Component.extend({
	'tagName': '',
    'index': 0,
    'total': 1,
	'selected': [],

    'zIndex': function() {
		var total = this.get('total'),
			index = this.get('index');

		return parseInt(total) - parseInt(index);
    }.property('index', 'total'),

	'isSelected': function() {
		var selected = this.get('selected'),
			orderId = this.get('order.id'),
			found = false;

      selected.forEach(function (selectedItem) {
      	if (selectedItem.id == orderId) {
      		found = true;
      	}
      });
			return found;
	}.property('selected.[]', 'order.id'),

    'actions': {
		'toggleItem': function() {

			var selected = this.get('isSelected'),
				orderId = this.get('order.id'),
				total = this.get('order.total');

			this.sendAction('selectAction', orderId, total, (!selected));
		}
    }
});
