EmberApp.PmFpFullordersItemComponent = Ember.Component.extend({
  'tagName': 'li',
  'classNames':['row'],
  'selected': [],

  'didInsertElement': function() {
    var captured = this.get('order.payment_status');

    if (captured == 'Captured' ) {
      this.set('captured', true);
    } else {
      this.set('captured', false);
    }
  },

  'isSelected': function() {
    var selected = this.get('selected'),
        orderId = this.get('order.order_id'),
        found = false;

    selected.forEach(function (selectedItem) {
      	if (selectedItem.id == orderId) {
      		found = true;
      	}
      });
			return found;
    return (selected.indexOf(orderId) > -1);
  }.property('selected.[]', 'order.id'),

    'actions': {
      'toggleItem': function() {

        var selected = this.get('isSelected'),
          orderId = this.get('order.order_id'),
          total = this.get('order.total');

          // console.log();
        this.sendAction('selectAction', orderId, total, (!selected));
      },

      'fraudRecheck': function() {

      }
    }

});
