/**
 * Payment Manager Orders List
 */
EmberApp.PmFpOrderListComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'pm-product-list', 'table-list', 'products' ],
	  'selectedItems': [],
	  'currentPage': 1,
    'itemsPerPage': 25,
    'itemsToShow': [],

    'didInsertElement': function() {
      this.changePage(1, this.get('itemsPerPage'));
    },

    /**
     * returns the number of pages to display
     *
     * @method maxPages
     * @return {Number}
     */
    'maxPages': function() {
        var orders = this.get('orders'),
            itemsPerPage = this.get('itemsPerPage');
        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('orders', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {
      this.set('itemsPerPage', itemsPerPage);
      var controller = this,
          orders = this.get('orders'),
          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1;
          items = orders.slice(start, end + 1);

       controller.set('itemsToShow', items);
    },

    'actions': {
       /**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
       'changePage': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);
      	},

        'backToOrders': function() {
  			  window.location.reload();
  		  },

  		'toggleAll': function() {
  			var selectAll = this.get('selectAll'),
  				selected = this.get('selectedItems');

  			if (selectAll) {
  				selected.clear();
  			} else {
  				var orders = this.get('orders');
  				orders.forEach(function(order) {
  					var id = order.order_id;

  					if (selected.indexOf(id) < 0) {
  						selected.pushObject(id);
  					}
  				});
  			}
  			this.set('selectAll', (!selectAll)).set('selectedItems', selected);

  		},

  		'selectItem': function(orderId, selected) {

  			var selectedItems = this.get('selectedItems');

  			if (selected) {
  				if (selectedItems.indexOf(orderId) < 0) {
  					selectedItems.pushObject(orderId);
  					this.set('selectedItems', selectedItems);
  				}
  				if (selectedItems.length == this.get('orders').length) {
  					this.set('selectAll', (!this.get('selectAll')));
  				}

  			} else {
  				if (selectedItems.indexOf(orderId) > -1) {
  					selectedItems.removeObject(orderId);
  					this.set('selectedItems', selectedItems);
  				}
  				if (this.get('selectAll')) {
  					this.set('selectAll', (!this.get('selectAll')));
  				}
  			}
  		},

  		'chargeConfirm': function() {
  			var url = '/customer/apps/paymentmanager/authorizeConfirm';
  			var controller = this;
  			Ember.$.ajax({
  				'url': url,
  				'type': 'GET',
  				'data': {
  							'orders': controller.get('selectedItems'),
  						},
  				'dataType': 'json'
  			}).done(function(res) {
  				controller.sendAction('ordersForAuthorize', res);
  			});
		  }

    } // End of actionss
});
