/**
 * Payment Manager Orders List
 */
EmberApp.PmAuthorizeResponseListComponent = Ember.Component.extend({
  'tagName': 'ul',
  'classNames': [ 'pm-product-list', 'table-list', 'products' ],
	'selectedItems': [],
	'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },
	'emailTemplates': 	[],
	'selectedTemplate': '',
	'listName': '',
	'currentPage': 1,
  'itemsPerPage': 25,
  'itemsToShow': [],

  'didInsertElement': function() {
    this.changePage(1, this.get('itemsPerPage'));
    // this.sendAction('changePage', 1);
  },

    /**
     * returns the number of pages to display
     *
     * @method maxPages
     * @return {Number}
     */
    'maxPages': function() {
        var orders = this.get('orders'),
            itemsPerPage = this.get('itemsPerPage');
        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('orders', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {
      this.set('itemsPerPage', itemsPerPage);
      var controller = this,
          orders = this.get('orders'),
          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1;
          items = orders.slice(start, end + 1);

      controller.set('itemsToShow', items);
    },

  'actions': {
    	/**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
    'changePage': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);
      	},

		'toggleAll': function() {
			var selectAll = this.get('selectAll'),
				selectedItems = this.get('selectedItems');

			if (selectAll) {
				selectedItems.clear();
			} else {
				var orders = this.get('orders');
				orders.forEach(function(order) {
					var id = order.order_id;

					if (selectedItems.indexOf(id) < 0) {
						selectedItems.pushObject(id);
					}
				});
			}
			this.set('selectAll', (!selectAll)).set('selectedItems', selectedItems);
		},

		'selectItem': function(orderId, selected) {
			var controller = this;
			var selectedItems = controller.get('selectedItems');

			if (selected) {
				if (selectedItems.indexOf(orderId) < 0) {
					selectedItems.pushObject(orderId);
					this.set('selectedItems', selectedItems);
				}
				if (selectedItems.length == this.get('orders').length) {
					this.set('selectAll', (!this.get('selectAll')));
				}

			} else {
				if (selectedItems.indexOf(orderId) > -1) {
					selectedItems.removeObject(orderId);
					this.set('selectedItems', selectedItems);
				}
				if (this.get('selectAll')) {
					this.set('selectAll', (!this.get('selectAll')));
				}
			}
		},

		'saveAuthList': function() {
			var url = '/customer/apps/paymentmanager/saveAuthList';
			var controller = this;
			// filter out all the successed orders
			// var successedOrders = this.get('orders').filter(function(order) {
			// 	if (order.payment_status == 'Success') {
			// 		return true;
			// 	}
			// });

			Ember.$.ajax({
				'url': url,
				'type': 'POST',
				'data': {
							'orders': this.get('orders'),
							'listName': this.get('listName')
						},
				'dataType': 'json'
			}).done(function(res) {
				if (res.status == 'success') {
					controller.set('authListPopup', false);
				}
				// controller.sendAction('setAuthorizeResponseList', res);
			});
		},

		'newSearch': function() {
			window.location.reload();
		},

		'sendEmail': function() {
			// Filter out all the failed orders
			var failedOrders = this.get('orders').filter(function(order) {
				if (order.payment_status == 'Failed') {
					return true;
				}
			});

			var url = '/customer/apps/paymentmanager/sendEmails';

			var controller = this;
			Ember.$.ajax({
				'url': url,
				'type': 'POST',
				'data': {
					'failedOrders': failedOrders,
					'template': controller.get('selectedTemplate')
					 }
			}).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                        	controller.set('emailFailedPopup', false);
                           	controller.notifications.show('Emails are sent!', 'Success', 'success');
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.', 'Error', 'error');
                     });
		},

		'openAuthorizeSavingPopup': function() {
			this.set('authListPopup', true);
		},

		'closeAuthorizeSavingPopup': function() {
			this.set('authListPopup', false);
		},

		'openEmailFailedPopup': function() {
			this.set('emailFailedPopup', true);
			var url = '/customer/apps/paymentmanager/getEmailTemplates';
			controller = this;
			Ember.$.ajax({
				'url': url,
				'type': 'GET',
				'data': { 'onlyNames': true },
				'dataType': 'json'
			}).done(function(res) {
				controller.set('emailTemplates', res);
			});
		},

		'closeEmailFailedPopup': function() {
			this.set('emailFailedPopup', false);
		},

  }

});
