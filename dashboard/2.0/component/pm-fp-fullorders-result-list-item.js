/**
* Payment Manager Orders List Item
*/
EmberApp.PmFpFullordersResultListItemComponent = Ember.Component.extend({
 'tagName': 'li',
 'classNames':['row'],
 'index': 0,
 'total': 1,
 'selected': [],

  'didInsertElement': function () {
    if (this.get('method') == 'authorize') {
      this.set('authorize', true);
    }
  },

 'isSelected': function() {
   var selected = this.get('selected'),
     orderId = this.get('order.order_id');

   return (selected.indexOf(orderId) > -1);
 }.property('selected.[]', 'order.order_id'),

   'actions': {
   'toggleItem': function() {
     var selected = this.get('isSelected'),
       orderId = this.get('order.order_id');
     this.sendAction('selectAction', orderId, (!selected));
   }
   }
});
