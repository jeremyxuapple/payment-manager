/**
 * Payment Manager Orders List
 */
EmberApp.PmAuthorizeListComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'pm-product-list', 'table-list', 'products' ],
	'selectedItems': [],
	'currentPage': 1,
    'itemsPerPage': 25,
    'itemsToShow': [],
    'authorizing': false,

    'didInsertElement': function() {
      this.changePage(1, this.get('itemsPerPage'));
      // this.sendAction('changePage', 1);
    },

    /**
     * returns the number of pages to display
     *
     * @method maxPages
     * @return {Number}
     */
    'maxPages': function() {
        var orders = this.get('orders'),
            itemsPerPage = this.get('itemsPerPage');
        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('orders', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {
      this.set('itemsPerPage', itemsPerPage);
      var controller = this,
          orders = this.get('orders'),
          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1;
          items = orders.slice(start, end + 1);

       controller.set('itemsToShow', items);
    },

    'actions': {
    	 /**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
      	'changePage': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);
      	},

      	'newSearch': function() {
			window.location.reload();
		},

		'toggleAll': function() {
			var selectAll = this.get('selectAll'),
				selectedItems = this.get('selectedItems');

			if (selectAll) {
				selectedItems.clear();
			} else {
				var orders = this.get('orders');
				orders.forEach(function(order) {
					if (selectedItems.length == 0) {
						selectedItems.pushObject({
							 	'id': order.id,
							 	'total': order.total
							 });
					} else {
						for (var i = 0; i < selectedItems.length; i++) {
							if (selectedItems[i].id == order.id) {
								break;
							} else if (i == selectedItems.length - 1) {
								selectedItems.pushObject({
								 	'id': order.id,
								 	'total': order.total
								 });
							}
						}
					}
				});
			}
			this.set('selectAll', (!selectAll)).set('selectedItems', selectedItems);

		},

		'selectItem': function(orderId, total, selected) {
			var controller = this;
			var selectedItems = controller.get('selectedItems');

			if (selected) {
				// When the selectedItems array is empty, push the order directly
				if (selectedItems.length == 0) {
						selectedItems.pushObject({
							 	'id': orderId,
							 	'total':total
							 });
					// otherwise check if the order is already in the selectedItems array
					} else {
						for (var i = 0; i < selectedItems.length; i++) {
							if (selectedItems[i].id == orderId) {
								break;
							} else if (i == selectedItems.length - 1) {
								selectedItems.pushObject({
								 	'id': orderId,
								 	'total': total
								 });
							}
						}
					}

				// when all the items has been selected, we will update the selecteAll property
				if (selectedItems.length == controller.get('orders').length) {
					controller.set('selectAll', (!controller.get('selectAll')));
				}

			} else {
				selectedItems.forEach(function(selectedItem) {
					if (selectedItem.id == orderId) {
						selectedItems.removeObject(selectedItem);
						// if the toggle all has been checked, we need to unchecked it
						if (controller.get('selectAll')) {
							controller.set('selectAll', (!controller.get('selectAll')));
						}
					}
				});
			}
			controller.set('selectedItems', selectedItems);
		},

		'authorizeOrders': function() {
			var url = '/customer/apps/paymentmanager/authorizeOrders';
			var controller = this;

      if (window.location.href.includes('fabpro')) {
        var company = 'fabpro';
      } else {
        var company = 'gg';
      }

			controller.set('authorizing', true);
			Ember.$.ajax({
				'url': url,
				'type': 'POST',
				'data': {
							'orders': this.get('selectedItems'),
							'company': company
						},
				'dataType': 'json'
			}).done(function(res) {
				controller.set('authorizing', false);
				controller.sendAction('authorizeResponseList', res);
			});
		},

		'exportCSV': function() {

			var url = '/customer/apps/paymentmanager/searchOrder';
			var controller = this;
			Ember.$.ajax({
				'url': url,
				'type': 'GET',
				'data': { 'exportCSV': true },
				'dataType': 'text'
			}).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                           	controller.notifications.show('Download will start soon.', 'Success', 'success');
                           	var url = "data:text/csv,"+encodeURIComponent(resp);
                           	var a = $("<a />", {
                              	href: url,
                              	download: "searchResult.csv"
                           	})
                           	.appendTo("body")
                           	.get(0)
                           	.click();
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.', 'Error', 'error');
                     });
			}
    }
});
