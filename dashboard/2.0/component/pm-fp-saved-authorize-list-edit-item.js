/**
 * Payment Manager Saved Authorize List Item Edit Component for controlling
 *	the logic of item adjustment or cancellation
 */

EmberApp.PmFpSavedAuthorizeListEditItemComponent = Ember.Component.extend({
	'tagName': '',
    'canceled': false,

    'didInsertElement': function() {
      if (this.get('order').payment_status == 'Void') {
        this.set('canceled', true);
      }
    },

    'actions': {
    	'adjustItem': function() {
    		var total = this.get('order').auth_total,
                id = this.get('order').id;
    		this.sendAction('adjustPopupFlag', id, total, true);
    	},

    	'cancelItem': function() {
    		var order = this.get('order');
            var id = order.id;
            this.sendAction('cancelPopupFlag', id);
    	}
    }
});
