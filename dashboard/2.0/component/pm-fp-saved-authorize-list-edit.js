EmberApp.PmFpSavedAuthorizeListEditComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'pm-product-list', 'table-list', 'products' ],
    'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },
    'selectDOMName': 'unprocessed',
    'processed': false,
    'unprocessedSection': false,
    'adjustedTotal': '',
    'currentPage': 1,
    'processedCurrentPage': 1,
    'itemsPerPage': 25,
    'processedItemsPerPage': 25,
    'unprocessedItems':[],
    'processedItems':[],
    'itemsToShow': [],
    'processedItemsToShow': [],
    'unprocessed': true,

    'didInsertElement': function() {
      var orders = this.get('model');

      var unprocessedItems = orders.filter(function(order) {
        if (order.payment_status == 'Void' || order.payment_status == 'Authorized' || order.payment_status == 'Failed') {
          return true;
        }
      });

      if (unprocessedItems.length > 0) {
        this.set('unprocessedSection', true);
        this.set('unprocessedItems', unprocessedItems);
      }

      var processedItems = orders.filter(function(order) {
        if (order.payment_status == 'Captured' ) {
          return true;
        }
      });

      if (processedItems.length > 0) {
        this.set('processedItems', processedItems);
        this.set('processed', true);
      }

      this.changePage(1, this.get('itemsPerPage'));
      this.processedChangePage(1, this.get('processedItemsPerPage'));
    },

    'maxPages': function() {
      var orders = this.get('unprocessedItems'),
          itemsPerPage = this.get('itemsPerPage');

        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('unprocessedItems', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {

      this.set('itemsPerPage', itemsPerPage);
      var controller = this,
          orders = this.get('unprocessedItems'),

          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1,

          items = orders.slice(start, end + 1);

      controller.set('itemsToShow', items);
    },

     'processedMaxPages': function() {
        var orders = this.get('processedItems'),
            processedItemsPerPage = this.get('processedItemsPerPage');
        if (orders) return Math.ceil(orders.length / processedItemsPerPage);
        else return 1;
    }.property('processedItems', 'processedItemsPerPage'),

    'processedChangePage': function(pageNum, processedItemsPerPage) {

      this.set('processedItemsPerPage', processedItemsPerPage);

      var controller = this,
          orders = this.get('processedItems'),
          pageNum = parseInt(pageNum),
          processedItemsPerPage = parseInt(processedItemsPerPage),

          start = (pageNum - 1) * processedItemsPerPage,
          end = start + processedItemsPerPage - 1,

          items = orders.slice(start, end + 1);

      controller.set('processedItemsToShow', items);
    },

    'actions': {
       /**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
      'changePage1': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);
      },

      'processedChangePage': function(pageNum, processedItemsPerPage) {
          this.processedChangePage(pageNum, processedItemsPerPage);
      },

      'cancelAll': function() {
          var unprocessedItems = this.get('unprocessedItems');
          var orders = unprocessedItems.filter(function(order) {
            if (order.payment_status == 'Authorized') {
              return true;
            }
          });

          var url = '/customer/apps/paymentmanager/cancelAll';
          var controller = this;

          Ember.$.ajax({
              'url': url,
              'type': 'POST',
              'data': {
                        'orders': orders,
                      },
              'dataType': 'json'
          }).success(function(resp) {
                      try {
                         if(JSON.parse(resp).success == false){
                            controller.notifications.show('No information was found.', 'Success', 'success');
                         }else{
                            controller.notifications.show('Failed to void transaction.', 'Error', 'error');
                         }
                      } catch (e) {
                          controller.notifications.show('Transaction Void Successfully', 'Success', 'success');
                          window.location.reload();
                      }
                   }).fail(function() {
                      controller.notifications.show('Failed to void transaction.', 'Error', 'error');
                   });
      },

      'captureAll': function() {

        var unprocessedItems = this.get('unprocessedItems');
          var orders = unprocessedItems.filter(function(order) {
            if (order.payment_status == 'Authorized') {
              return true;
            }
          });

        var url = '/customer/apps/paymentmanager/captureAll';
        var controller = this;

        Ember.$.ajax({
            'url': url,
            'type': 'POST',
            'data': {
                      'orders': orders,
                      'company': 'fabpro'
                    },
            'dataType': 'json'
        }).success(function(resp) {
                    try {
                       if(JSON.parse(resp).success == false){
                          controller.notifications.show('No information was found.', 'Success', 'success');
                       }else{
                          controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
                       }
                    } catch (e) {
                        controller.notifications.show('Capture transactions successfully', 'Success', 'success');
                        window.location.reload();
                    }
                 }).fail(function() {
                    controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
                 });
      },

      'cancelAllPopup': function() {
        this.set('cancelAllPopup', true);
      },

      'closeCancelAllPopup': function() {
        this.set('cancelAllPopup', false);
      },

      'captureAllPopup': function() {
        this.set('captureAllPopup', true);
      },

      'closeCaptureAllPopup': function() {
        this.set('captureAllPopup', false);
      },

    	'adjustPopup': function(id, total, flag) {
    		this.set('adjustPopup', flag);
    		this.set('adjustAllowLimit', total);
        this.set('adjustedId', id);
    	},

    	'closeAdjustPopup': function() {
    		this.set('adjustPopup', false);
    	},

      'emailPopup': function() {
          this.set('emailFailedPopup', true);
          var url = '/customer/apps/paymentmanager/getEmailTemplates';
          controller = this;
          Ember.$.ajax({
            'url': url,
            'type': 'GET',
            'data': { 'onlyNames': true },
            'dataType': 'json'
          }).done(function(res) {
            controller.set('emailTemplates', res);
          });
      },

      'closeEmailPopup': function() {
        this.set('emailFailedPopup', false);
      },

    	'saveAdjustment': function() {
            var id = this.get('adjustedId'),
                adjustedTotal = this.get('adjustedTotal');
            var url = '/customer/apps/paymentmanager/adjustAuthItem';
            var controller = this;

            Ember.$.ajax({
                'url': url,
                'type': 'POST',
                'data': {
                          'id': id,
                          'adjustedTotal': adjustedTotal
                        },
                'dataType': 'json'
            }).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                            controller.notifications.show('Adjust success', 'Success', 'success');
                            controller.set('adjustPopup', false);
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.', 'Error', 'error');
                     });
    	},

      'cancelPopup': function(id) {
          this.set('cancelItemId', id);
          this.set('cancelPopup', true);
      },

      'closeCancelPopup': function() {
          this.set('cancelPopup', false);
      },

      'cancelTransaction': function() {
          var cancelItemId = this.get('cancelItemId');
          var url = '/customer/apps/paymentmanager/cancelAuthItem';
          var controller = this;

          Ember.$.ajax({
              'url': url,
              'type': 'POST',
              'data': {
                        'id': cancelItemId,
                      },
              'dataType': 'json'
          }).success(function(resp) {

                      try {
                         if(resp.success == false){
                            controller.notifications.show('No information was found.', 'Success', 'success');
                         }else{

                            controller.notifications.show('Transaction Canceled', 'Success', 'success');
                            controller.set('cancelPopup', false);
                         }
                      } catch (e) {

                      }
                   }).fail(function() {
                      controller.notifications.show('Failed to download.', 'Error', 'error');
                   });
      },

      'exportAuthCSV': function() {
        var listId = this.get('model')[0].pm_auth_list_id;
        var url = '/customer/apps/paymentmanager/pmAuthlistsEdits/' + listId;
        var controller = this;

        Ember.$.ajax({
            'url': url,
            'type': 'GET',
            'data': {
              'export': true
            },
            'dataType': 'text'
        }).success(function(resp) {
                      try {
                         if(JSON.parse(resp).success == false){
                            controller.notifications.show('No information was found.', 'Success', 'success');
                         }else{
                            controller.notifications.show('Failed to download.', 'Error', 'error');
                         }
                      } catch (e) {
                          controller.notifications.show('Download will start soon.', 'Success', 'success');
                          var url = "data:text/csv,"+encodeURIComponent(resp);
                          var a = $("<a />", {
                              href: url,
                              download: "Authorization Result.csv"
                          })
                          .appendTo("body")
                          .get(0)
                          .click();
                      }
                   }).fail(function() {
                      controller.notifications.show('Failed to download.werewsr', 'Error', 'error');
                   });
      },

      'exportCaptureCSV': function() {
          var orders = this.get('processedItems');
          var url = '/customer/apps/paymentmanager/exportCaptureReport';
          var controller = this;

          orders.forEach(function(order) {
            delete order['id'];
            delete order['date_created'];
            delete order['date_updated'];
            delete order['product_sku'];
            delete order['bc_customer_id'];
            delete order['customer_id'];
            delete order['product_name'];
            delete order['product_qty'];
            delete order['gateway'];
            delete order['pm_auth_list_id'];
          });

          Ember.$.ajax({
              'url': url,
              'type': 'GET',
              'data': {
                        'orders': orders,
                      },
              'dataType': 'text'
          }).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                            controller.notifications.show('Download will start soon.', 'Success', 'success');
                            var url = "data:text/csv,"+encodeURIComponent(resp);
                            var a = $("<a />", {
                                href: url,
                                download: "Capture Result.csv"
                            })
                            .appendTo("body")
                            .get(0)
                            .click();
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.werewsr', 'Error', 'error');
                     });
      },

      'emailFailed': function() {
          // Filter out all the failed orders
          var failedOrders = this.get('processedItems').filter(function(order) {
            if (order.payment_status == 'Failed') {
              return true;
            }
          });

          var url = '/customer/apps/paymentmanager/sendEmails';

          var controller = this;
          Ember.$.ajax({
          'url': url,
          'type': 'POST',
          'data': {
            'failedOrders': failedOrders,
            'template': controller.get('selectedTemplate')
             }
          }).success(function(resp) {
                          try {
                             if(JSON.parse(resp).success == false){
                                controller.notifications.show('No information was found.', 'Success', 'success');
                             }else{
                                controller.notifications.show('Failed to download.', 'Error', 'error');
                             }
                          } catch (e) {
                            controller.set('emailFailedPopup', false);
                              controller.notifications.show('Emails are sent!', 'Success', 'success');
                          }
                       }).fail(function() {
                          controller.notifications.show('Failed to download.', 'Error', 'error');
                       });
          }
      } // End of actions
});
