 /**
 * Payment Manager Orders List Item
 */
EmberApp.PmFpOrderListItemComponent = Ember.Component.extend({
	'tagName': '',
  'index': 0,
  'total': 1,
	'selected': [],

    'zIndex': function() {
			var total = this.get('total'),
				index = this.get('index');

			return parseInt(total) - parseInt(index);
    }.property('index', 'total'),

	'isSelected': function() {
		var selected = this.get('selected'),
			orderId = this.get('order.order_id');

		return (selected.indexOf(orderId) > -1);
	}.property('selected.[]', 'order.order_id'),

    'actions': {
			'toggleItem': function() {
				var selected = this.get('isSelected'),
					orderId = this.get('order.order_id'),
					total = this.get('order.total');
				this.sendAction('selectAction', orderId, total, (!selected));
			}
    }
});
