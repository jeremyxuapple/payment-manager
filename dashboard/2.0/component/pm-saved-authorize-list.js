EmberApp.PmSavedAuthorizeListComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'pm-product-list', 'table-list', 'products' ],

    'currentPage': 1,
    'itemsPerPage': 25,
    'itemsToShow': [],
    
    'didInsertElement': function() {
      this.changePage(1, this.get('itemsPerPage'));
      // this.sendAction('changePage', 1);
    },

    /**
     * returns the number of pages to display
     *
     * @method maxPages
     * @return {Number}
     */
    'maxPages': function() {
        var orders = this.get('model'),
            itemsPerPage = this.get('itemsPerPage');
        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('model', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {
      this.set('itemsPerPage', itemsPerPage);

      var controller = this,
          orders = this.get('model'),
          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1,
          items = orders.slice(start, end + 1);
         
      controller.set('itemsToShow', items);
    },

    'actions': {
    	/**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
      'changePage': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);          
      },
    }
});