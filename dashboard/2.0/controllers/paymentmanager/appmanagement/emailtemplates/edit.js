EmberApp.PaymentmanagerAppmanagementEmailtemplatesEditController = Ember.Controller.extend({ 

	'actions': {
		'saveEmail': function() {

			var template = this.get('model');
			var id = template.id;
			var url = '/customer/apps/paymentmanager/saveEmailTemplate/' + id;
            var controller = this;

            // Formatting the data for template creation. 
            if ( template.creating ) {
            	var url = '/customer/apps/paymentmanager/saveEmailTemplate/' + 0;
            	delete template.creating;
            	template.date_created = template.date_updated = Math.round((new Date()).getTime() / 1000);
            } else {
            	delete template.date_created;
            	template.date_updated = Math.round((new Date()).getTime() / 1000);
            }

            Ember.$.ajax({
                'url': url,
                'type': 'POST',
                'data': {
                          'template': template
                        },
                'dataType': 'json'
            }).success(function(resp) {
                        try {
                           if(JSON.parse(resp).success == false){
                              controller.notifications.show('No information was found.', 'Success', 'success');
                           }else{
                              controller.notifications.show('Failed to download.', 'Error', 'error');
                           }
                        } catch (e) {
                            controller.notifications.show('Email Template Saved!', 'Success', 'success');
                        }
                     }).fail(function() {
                        controller.notifications.show('Failed to download.', 'Error', 'error');
                     });
		}
	}
});