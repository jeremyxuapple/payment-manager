EmberApp.PaymentmanagerSearchfabproController = Ember.Controller.extend({

	'Loading': true,
	'searchInProgress': false,
	'noResults': false,
	'searchSection': true,
	'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },
	'paymentStatus': [
		{ id : 1, label : "Pending" },
		{ id : 2, label : "Shipped" },
		{ id : 3, label : "Awaiting Payment" },
		{ id : 4, label : "Completed" },
		{ id : 5, label : "Awaiting Fulfillment" },
		{ id : 6, label : "Pending Pre-Order" },
	],

	'buildSearchTerm': function() {
		var order = this.get('model');
		var orderID =  order.get('orderID'),
			orderStatus = order.get('selectedOrderStatus').label;

		var optionsSearchFields = [
				{'key': 'Start ID: ', 'value': order.get('startID')},
				{'key': 'End ID: ', 'value': order.get('endID')},
				{'key': 'Order Status: ', 'value': orderStatus}
			];

		if (orderID) {
			return 'Order: ' + orderID;
		} else {
			var searchTerms = '';
			optionsSearchFields.forEach(function(field) {
				if (field.value) {
					searchTerms += field.key;
					searchTerms += field.value;
					searchTerms += ' ';
				}
			});
			return searchTerms;
		}
	},

	'actions': {
		'searchOrder': function() {

			var controller = this;
			var order = controller.get('model');
				controller.set('searchInProgress', true);
				controller.set('noResults', false);

			if (order.get('selectedOrderStatus') != undefined) {

				if (order.get('selectedOrderStatus').label == 'Pending Pre-Order')
					var orderStatus = { id : 6, label : "Awaiting Pickup" };

			} else {
				var orderStatus = order.get('selectedOrderStatus');
			}

			var url = '/customer/apps/paymentmanager/searchOrderFabpro';

			Ember.$.ajax({
				'url':url,
				'type': 'GET',
				'data': {
					'orderID': order.get('orderID'),
					'startID': order.get('startID'),
					'endID': order.get('endID'),
					'orderStatus': orderStatus,
				},
				'dataType': 'json'
			}).done(function(res) {

				if (res.length == 0) {
					controller.set('noResults', true);
				}
					controller.set('orders', res);
					controller.set('searchSection', false);
					controller.set('searchInProgress', false);
					controller.set('searchTerms', controller.buildSearchTerm());

			}).fail(function(err) {
				alert(err.responseJSON.msg);
			})
		}, // End of the searchOrder function

		'setAuthroizeOrders': function(ordersForAuthorize) {
			this.set('ordersForAuthorize', ordersForAuthorize);
			this.set('orders', false);
		},

		'setAuthorizeResponseList': function(authorizeResponseList) {
			this.set('authorizeResponseList', authorizeResponseList);
			this.set('ordersForAuthorize', false);
		},

		'newSearch': function() {
			window.location.reload();
		},

	}
});
