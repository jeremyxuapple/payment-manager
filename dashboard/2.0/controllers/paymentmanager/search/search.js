EmberApp.PaymentmanagerSearchController = Ember.Controller.extend({

	'Loading': true,
	'searchInProgress': false,
	'noResults': false,
	'searchSection': true,
	'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },
	'paymentStatus': [
		{ id : 1, label : "Pending" },
		{ id : 2, label : "Shipped" },
		{ id : 3, label : "Awaiting Payment" },
		{ id : 4, label : "Completed" },
		{ id : 5, label : "Awaiting Fulfillment" },
		{ id : 6, label : "Awaiting Pickup" },
	],
	'orderTypes': 	[
		{ id : 0, label : "Single Product" },
		{ id : 1, label : "Multiple Product" },
		{ id : 2, label : "All" },
	],
	'productToolTip': 'Product Name is a required field if you want to do order range search.',

	'getProductNames': function() {
		var url = '/customer/apps/paymentmanager/searchProductNames';
		var controller = this;

		Ember.$.ajax({
			'url': url,
			'type': 'GET',
			'dataType': 'json'
		}).done(function (res) {

			res.forEach(function(name){
				name.label = name.label + ' -- SKU: ' + name.sku;
			});

			controller.set('productNames', res);
			controller.set('Loading', false);
		});

	}.on('init'),

	'buildSearchTerm': function() {
		var order = this.get('model');
		var orderID =  order.get('orderID'),
		    productName = order.get('productName') ? order.get('productName').label : '',
			orderStatus = order.get('selectedOrderStatus') ? order.get('selectedOrderStatus').label : '',
			orderType = order.get('selectedOrderType') ? order.get('selectedOrderType').label : '';

		var optionsSearchFields = [
				{'key': 'Start ID: ', 'value': order.get('startID')},
				{'key': 'End ID: ', 'value': order.get('endID')},
				{'key': 'Order Status: ', 'value': orderStatus},
				{'key': 'order Type: ', 'value': orderType}
			];

		if (orderID) {
			return 'Order: ' + orderID;
		} else {
			var searchTerms = 'Product: ' + productName + ' ';
			optionsSearchFields.forEach(function(field) {
				if (field.value) {
					searchTerms += ', ';
					searchTerms += field.key;
					searchTerms += field.value;
				}
			});
			return searchTerms;
		}
	},

	'actions': {
		'searchOrder': function() {

			var controller = this;
			var order = controller.get('model');
				controller.set('searchInProgress', true);
				controller.set('noResults', false);
			var productName = order.get('productName') ? order.get('productName').label : '';
			var url = '/customer/apps/paymentmanager/searchOrder';

			Ember.$.ajax({
				'url':url,
				'type': 'GET',
				'data': {
					'orderID': order.get('orderID'),
					'startID': order.get('startID'),
					'endID': order.get('endID'),
					'productName': productName,
					'orderStatus': order.get('selectedOrderStatus'),
					'orderType': order.get('selectedOrderType')
				},
				'dataType': 'json'
			}).done(function(res) {

				if (res.length == 0) {
					controller.set('noResults', true);
				}
					controller.set('orders', res);
					controller.set('searchSection', false);
					controller.set('searchInProgress', false);
					controller.set('productName', order.get('productName'));
					controller.set('searchTerms', controller.buildSearchTerm());

			}).fail(function(err) {
				alert(err.responseJSON.msg);
			})
		}, // End of the searchOrder function

		'setAuthroizeOrders': function(ordersForAuthorize) {
			this.set('ordersForAuthorize', ordersForAuthorize);
			this.set('orders', false);
		},

		'setAuthorizeResponseList': function(authorizeResponseList) {
			this.set('authorizeResponseList', authorizeResponseList);
			this.set('ordersForAuthorize', false);
		},

		'newSearch': function() {
			window.location.reload();
		},

		'cornJob': function() {
			var url = '/customer/apps/paymentmanager/cornJob';
			var controller = this;

			Ember.$.ajax({
				'url': url,
				'type': 'POST',
				'dataType': 'json'
			}).done(function (res) {


			});
		}

	}
});
