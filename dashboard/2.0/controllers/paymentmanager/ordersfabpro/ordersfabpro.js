EmberApp.PaymentmanagerOrdersfabproController = Ember.Controller.extend({

  'itemsPerPage': 25,
  'currentPage': 1,
  'product_name':'',
  'payment_status':'',
  'selectedItems':[],
  'authorizing': false,
  'orderList': true,
  'resultList': false,
  'sortById': {'sortFlag': false, 'asc': false},
  'sortByDate': {'sortFlag': false, 'asc': false},
  'sortByType': {'sortFlag': false, 'asc': false},
  'sortByPaymentStatus':{'sortFlag': false, 'asc': false},
  'sortyByFraudStatus': {'sortFlag': false, 'asc': false},
  'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },
  'paymentStatus':[
    {'label': 'Fully Authorized', 'value': 'Fully Authorized'},
    {'label': '$0 Authorized', 'value': '$0 Authorized'},
    {'label': 'Captured', 'value': 'Captured'},
    {'label': 'Failed Capture', 'value': 'Failed Capture'},
    {'label': 'Failed Authorization', 'value': 'Failed Authorization'}
  ],
  'maxPages': function() {
      var orders = this.get('model'),
          itemsPerPage = this.get('itemsPerPage');

      if (orders) {
        return Math.ceil(orders.length / itemsPerPage);
      } else {
        return 1;
      }

  }.property('orders', 'itemsPerPage'),

  'getProductNames': function() {
    var url = '/customer/apps/paymentmanager/searchProductNames';
    var controller = this;

    Ember.$.ajax({
      'url': url,
      'type': 'GET',
      'dataType': 'json'
    }).done(function (res) {

      controller.set('productNames', res);
      controller.set('Loading', false);
    });

  }.on('init'),

  'changePage': function(pageNum, itemsPerPage) {

    var controller = this;
    this.set('itemsPerPage', itemsPerPage);
    this.set('pageNum', pageNum);
    var orders = this.get('model');
    var ordersFiltered = this.updatePaymentStatus(orders);

    // Check if needs to sort the order by order ids

    if (this.get('sortById').sortFlag) {
      if (this.get('sortById').asc) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
            return parseFloat(a.order_id) - parseFloat(b.order_id);
        });
      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
            return parseFloat(b.order_id) - parseFloat(a.order_id);
        });
      }
    }

    if (this.get('sortByDate').sortFlag) {
      // Check if needs to sort the orders by the order date

      if (this.get('sortByDate').asc) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return Date.parse(a.date) - Date.parse(b.date);
        });

      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return Date.parse(b.date) - Date.parse(a.date);
        });
      }

    }

    if (this.get('sortByType').sortFlag) {
      // Check if needs to sort the orders by the order type
      if (this.get('sortByType').asc) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((a.type < b.type) ? -1 : ((a.type > b.type) ? 1 : 0));
        });
      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((b.type < a.type) ? -1 : ((b.type > a.type) ? 1 : 0));
        });
      }

    }

    // Check if needs to sort the orders by the order payemnt status

    if (this.get('sortByPaymentStatus').sortFlag) {
      if (this.get('sortByPaymentStatus').asc) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((a.payment_status < b.payment_status) ? -1 : ((a.payment_status > b.payment_status) ? 1 : 0));
        });
      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((b.payment_status < a.payment_status) ? -1 : ((b.payment_status > a.payment_status) ? 1 : 0));
        });
      }
    }

    // Check if needs to sort the orders by the order fraud status
    if (this.get('sortyByFraudStatus').sortFlag) {
      if (this.get('sortyByFraudStatus').asc) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((a.fraud_status < b.fraud_status) ? -1 : ((a.fraud_status > b.fraud_status) ? 1 : 0));
        });
      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
          return ((b.fraud_status < a.fraud_status) ? -1 : ((b.fraud_status > a.fraud_status) ? 1 : 0));
        });
      }
    }

    // Filter the payment status
    if (this.get('payment_status') != '') {
      // console.log(ordersFiltered);
      var paymentStatus = this.get('payment_status');
      ordersFiltered = ordersFiltered.filter(function(order) {
        if (order.payment_status == paymentStatus) return true;
      });
    }

    // Filter the orders base on the product name
    if (this.get('product_name') != '') {
      var productName = this.get('product_name').label;
      var url = '/customer/apps/paymentmanager/fabproFilterProductName';
      Ember.$.ajax({
          'url': url,
          'type': 'GET',
          'data': {
                    'productName': productName,
                  },
          'dataType': 'json'
      }).done(function (resp) {
        ordersFiltered = ordersFiltered.filter(function(itm){
          return resp.indexOf(itm.order_id) > -1;
        });
        var itemsPerPage = controller.get('itemsPerPage');
        var pageNum = controller.get('pageNum');

        controller.set('maxPages', Math.ceil(ordersFiltered.length / itemsPerPage));
        var pageNum = parseInt(pageNum),
            itemsPerPage = parseInt(itemsPerPage),
            start = (pageNum - 1) * itemsPerPage,
            end = start + itemsPerPage - 1,
            items = ordersFiltered.slice(start, end + 1);
        controller.set('itemsToShow', items);

      });
    }

    this.set('maxPages', Math.ceil(ordersFiltered.length / this.get('itemsPerPage')));
    var pageNum = parseInt(pageNum),
        itemsPerPage = parseInt(itemsPerPage),

        start = (pageNum - 1) * itemsPerPage,
        end = start + itemsPerPage - 1,
        items = ordersFiltered.slice(start, end + 1);

    this.set('itemsToShow', items);

  },

  // Update the payment status to full_authorized and 0 authorized
  'updatePaymentStatus': function(orders) {
    orders.forEach(function (order) {
      if(order.fraud_status == null) order.fraud_status = 'NA';

      if (order.payment_status == null) {
        if (order.type == 'Pre-Order') {
          order.payment_status = '$0 Authorized';
        } else if (order.type == 'In-Stock') {
          order.payment_status = 'Fully Authorized';
        } else {
          order.payment_status = 'NA';
        }
      }
    });
    return orders;
  },

  'sortOperation': function () {
    // Check if needs to sort the order by order ids
    if (this.get('sortById')) {
      ordersFiltered = ordersFiltered.sort(function(a, b) {
          return parseFloat(a.order_id) - parseFloat(b.order_id);
      });
    } else {
      ordersFiltered = ordersFiltered.sort(function(a, b) {
          return parseFloat(b.order_id) - parseFloat(a.order_id);
      });
    }

  },

  'actions': {
    'filterOrder': function() {
      this.changePage(1, this.get('itemsPerPage'));
      },

    'sortOperation': function (category) {

      this.sortOperation(category);

      if (this.get('sortFlag')) {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
            return parseFloat(a.parm) - parseFloat(b.parm);
        });
      } else {
        ordersFiltered = ordersFiltered.sort(function(a, b) {
            return parseFloat(b.parm) - parseFloat(a.parm);
        });
      }
    },

    'sortById':function () {
      this.set('sortByType.sortFlag', false);
      this.set('sortByDate.sortFlag', false);
      this.set('sortByPaymentStatus.sortFlag', false);
      this.set('sortyByFraudStatus.sortFlag', false);

      this.set('sortById.sortFlag', true);
      this.set('sortById.asc', !this.get('sortById.asc'));
      this.changePage(1, this.get('itemsPerPage'));
    },

    'sortByDate':function () {
      this.set('sortByType.sortFlag', false);
      this.set('sortByPaymentStatus.sortFlag', false);
      this.set('sortyByFraudStatus.sortFlag', false);
      this.set('sortById.sortFlag', false);

      this.set('sortByDate.sortFlag', true);
      this.set('sortByDate.asc', !this.get('sortByDate.asc'));
      this.changePage(1, this.get('itemsPerPage'));
    },

    'sortByType':function () {
      this.set('sortById.sortFlag', false);
      this.set('sortByPaymentStatus.sortFlag', false);
      this.set('sortyByFraudStatus.sortFlag', false);
      this.set('sortByDate.sortFlag', false);

      this.set('sortByType.sortFlag', true);
      this.set('sortByType.asc', !this.get('sortByType.asc'));
      this.changePage(1, this.get('itemsPerPage'));
    },

    'sortByPaymentStatus':function () {
      this.set('sortById.sortFlag', false);
      this.set('sortByType.sortFlag', false);
      this.set('sortyByFraudStatus.sortFlag', false);
      this.set('sortByDate.sortFlag', false);

      this.set('sortByPaymentStatus.sortFlag', true);
      this.set('sortByPaymentStatus.asc', !this.get('sortByPaymentStatus.asc'));
      this.changePage(1, this.get('itemsPerPage'));
    },

    'sortyByFraudStatus':function () {
      this.set('sortById.sortFlag', false);
      this.set('sortByType.sortFlag', false);
      this.set('sortByPaymentStatus.sortFlag', false);
      this.set('sortByDate.sortFlag', false);

      this.set('sortyByFraudStatus.sortFlag', true);
      this.set('sortyByFraudStatus.asc', !this.get('sortyByFraudStatus.asc'));
      this.changePage(1, this.get('itemsPerPage'));
    },

    'clearFilter': function () {
      this.set('product_name', '');
      this.set('payment_status', '');
      this.set('sortById.sortFlag', false);
      this.set('sortByType.sortFlag', false);
      this.set('sortByPaymentStatus.sortFlag', false);
      this.set('sortByDate.sortFlag', false);
      this.set('sortyByFraudStatus.sortFlag', false);
      this.changePage(1, this.get('itemsPerPage'));
    },

    'changePage': function(pageNum, itemsPerPage) {
       this.changePage(pageNum, itemsPerPage);
     },

    'toggleAll': function() {
 			var selectAll = this.get('selectAll'),
 				selectedItems = this.get('selectedItems');

 			if (selectAll) {
 				selectedItems.clear();
 			} else {
 				var orders = this.get('model');
 				orders.forEach(function(order) {

 					if (selectedItems.length == 0) {
 						selectedItems.pushObject({
 							 	'id': order.order_id,
 							 	'total': order.total
 							 });
 					} else {
 						for (var i = 0; i < selectedItems.length; i++) {
 							if (selectedItems[i].id == order.order_id) {
 								break;
 							} else if (i == selectedItems.length - 1) {
 								selectedItems.pushObject({
 								 	'id': order.order_id,
 								 	'total': order.total
 								 });
 							}
 						}
 					}
 				});
 			}

 			this.set('selectAll', (!selectAll)).set('selectedItems', selectedItems);

 		},

    'selectItem': function(orderId, total, selected) {
 			var controller = this;
 			var selectedItems = controller.get('selectedItems');

 			if (selected) {
 				// When the selectedItems array is empty, push the order directly
 				if (selectedItems.length == 0) {
 						selectedItems.pushObject({
 							 	'id': orderId,
 							 	'total':total
 							 });
 					// otherwise check if the order is already in the selectedItems array
 					} else {
 						for (var i = 0; i < selectedItems.length; i++) {
 							if (selectedItems[i].id == orderId) {
 								break;
 							} else if (i == selectedItems.length - 1) {
 								selectedItems.pushObject({
 								 	'id': orderId,
 								 	'total': total
 								 });
 							}
 						}
 					}

 				// when all the items has been selected, we will update the selecteAll property
 				if (selectedItems.length == controller.get('model').length) {
 					controller.set('selectAll', (!controller.get('selectAll')));
 				}

 			} else {
 				selectedItems.forEach(function(selectedItem) {
 					if (selectedItem.id == orderId) {
 						selectedItems.removeObject(selectedItem);
 						// if the toggle all has been checked, we need to unchecked it
 						if (controller.get('selectAll')) {
 							controller.set('selectAll', (!controller.get('selectAll')));
 						}
 					}
 				});
 			}
 			controller.set('selectedItems', selectedItems);
 		},

    'authorize': function() {
      this.set('orderList', false);
      this.set('authorizing', true);
      this.set('operation', 'authorize');

      var url = '/customer/apps/paymentmanager/fabproAuth';
      var controller = this;

      if (window.location.href.includes('fabpro')) {
        var company = 'fabpro';
      } else {
        var company = 'gg';
      }
      controller.set('authorizing', true);
      Ember.$.ajax({
        'url': url,
        'type': 'POST',
        'data': {
              'orders': this.get('selectedItems'),
              'company': company,
              'origin': window.location.origin,
            },
        'dataType': 'json'
      }).success(function(resp) {
                  try {
                     if(JSON.parse(resp).success == false){
                        controller.notifications.show('No information was found.', 'Success', 'success');
                     }else{
                        controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
                     }
                  } catch (e) {
                      controller.set('authorizing', false);
                      controller.set('resultList', true);
                      controller.set('responseList', resp);
                      controller.notifications.show('Authorize transactions successfully', 'Success', 'success');
                  }
               }).fail(function() {
                  controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
               });;
    },

    'capture': function() {
      this.set('orderList', false);
      this.set('operation', 'capture');

      var url = '/customer/apps/paymentmanager/fabproCapture';
      var controller = this;

      controller.set('authorizing', true);
      Ember.$.ajax({
          'url': url,
          'type': 'POST',
          'data': {
                    'orders': controller.get('selectedItems'),
                    'company': 'fabpro'
                  },
          'dataType': 'json'
      }).success(function(resp) {
                  try {
                     if(JSON.parse(resp).success == false){
                        controller.notifications.show('No information was found.', 'Success', 'success');
                     }else{
                        controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
                     }
                  } catch (e) {
                      controller.set('authorizing', false);
                      controller.set('resultList', true);
                      controller.set('responseList', resp);
                      controller.notifications.show('Capture transactions successfully', 'Success', 'success');
                  }
               }).fail(function() {
                  controller.notifications.show('Failed to capture transactions.', 'Error', 'error');
               });
    },
  } // End of Actions

});
