/**
 * Payment Manager Controller
 *
 * @class PaymentmanagerController
 * @namespace EmberApp
 * @extends Ember.Controller
 */

EmberApp.PaymentmanagerController = Ember.Controller.extend({
	'applicationCtrl': Ember.inject.controller('application')
});
