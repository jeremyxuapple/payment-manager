/** 
 * Payment Manager Model
 *
 * @class Payment Manager
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PaymentmanagerOrder = DS.Model.extend({
	'orderID' : DS.attr('number'), 
	'startID': DS.attr('number'),
	'endID': DS.attr('number'),
	'productName': DS.attr('string'),
	'selectedPaymentStatus': DS.attr('string'),
});

EmberApp.PaymentmanagerOrderAdapter = EmberApp.PaymentmanagerAPIAdapter.extend({});