/** 
 * Payment Manager Model
 *
 * @class Payment Manager
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PmAuthlists = DS.Model.extend({
	'name' : DS.attr('string'), 
	'status': DS.attr('string'),
	'orders_containing': DS.attr('number'),
	'date_created': DS.attr('string'),
	'orderList': DS.attr('')
});

EmberApp.PmAuthlistsAdapter = EmberApp.PaymentmanagerAPIAdapter.extend({});