EmberApp.PaymentmanagerAuthlistsEditRoute = Ember.Route.extend({
	'model': function(params) {
        return this.store.findRecord('pm-authlists-edit', params.id);
    },

    setupController: function(controller, model) {
        this._super(controller, model.get('orderLists'));
    },

});
