EmberApp.PaymentmanagerAppmanagementEmailtemplatesIndexRoute = Ember.Route.extend({
	'model': function() {
        return Ember.$.get('/customer/apps/paymentmanager/getEmailTemplates');
    },
    
    setupController: function(controller, model) {
    	controller.set('list', model.list);
    	controller.set('parts', model.parts);
        this._super(controller, model);
    },

});