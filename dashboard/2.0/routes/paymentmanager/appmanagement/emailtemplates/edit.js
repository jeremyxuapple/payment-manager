EmberApp.PaymentmanagerAppmanagementEmailtemplatesEditRoute = Ember.Route.extend({
	'model': function(params) {
        return Ember.$.get('/customer/apps/paymentmanager/getEmailTemplate/' + params.id);
    },
    
    setupController: function(controller, model) {
    	// console.log(model.objectAt(0));
    	// this._super(controller, model.objectAt(0));
        this._super(controller, model);
    },

});