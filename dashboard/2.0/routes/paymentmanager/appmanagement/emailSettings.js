EmberApp.PaymentmanagerAppmanagementEmailsettingsRoute = Ember.Route.extend({
	'model': function() {
        return Ember.$.get('/customer/apps/paymentmanager/getEmailSettings');
    },
    
    setupController: function(controller, model) {
    	controller.set('settings', model);
        this._super(controller, model);
    },

});