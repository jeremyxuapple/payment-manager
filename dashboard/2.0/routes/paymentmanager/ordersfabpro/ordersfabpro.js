EmberApp.PaymentmanagerOrdersfabproRoute = Ember.Route.extend({

	'model': function() {
      return Ember.$.get('/customer/apps/paymentmanager/ordersFabpro');
    },

    setupController: function(controller, model) {
        this._super(controller, model);
				controller.changePage(1, controller.get('itemsPerPage'));
    }
});
