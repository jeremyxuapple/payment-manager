/**
 * Payment Manager Route
 *
 * @class PaymentmanagerRoute
 * @namespace EmberApp
 * @extends Ember.Route
 *
*/
EmberApp.PaymentmanagerRoute = Ember.Route.extend({
    /**
     * retrieve application information from the data store
     *
     * @function model
     * @returns {RSVP.Promise}
     */

    model: function() {
    	return Ember.RSVP.all([
    		this.store.find('app', 'paymentmanager')
    	]);
    },

    afterModel:function(model) {
    	var applicationController = this.controllerFor('application');

        var menuItems = appMenuItems.get('paymentmanager');
        applicationController.set('appsMenu', menuItems);
    },

    setupController: function(controller, model){

    	this._super(model, controller);
    	var app = model.objectAt(0);

    	var menuItems = appMenuItems.get('paymentmanager');
        
    	controller
    		.set('app', app)
    		.set('appsMenu', menuItems);
    },

    actions: {
    	'clearSearch': function() {
    		 this.controller.send('clearSearch');
    	}
    }


});