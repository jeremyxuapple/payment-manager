
/**
* Payment Manager App Router
*/
EmberApp.Router.map(function() {
	this.resource('paymentmanager', { path: '/apps/paymentmanager' }, function() {
		this.route('search', { path: '/search' });
		this.route('searchfabpro', { path: '/searchfabpro' });
		this.route('authlists', { path: '/authlists' }, function() {
			this.route('edit', { path: '/:id' });
		});
		this.route('authlistsfabpro', { path: '/authlistsfabpro' }, function() {
			this.route('edit', { path: '/:id' });
		});
		this.route('ordersfabpro', { path: '/ordersfabpro' });
		this.route('appmanagement', { path: '/appmanagement' }, function() {
			this.route('emailsettings', { path: '/emailsettings'});
			this.route('emailtemplates', { path: '/emailtemplates'}, function() {
				this.route('edit', { path: '/:id'});
			});
		});
	});
});


/**
 * Adapter for Payment Manager Models
 *
 * @class PMAPIAdapter
 * @extends DS.RESTAdapter
 * @namspace EmberApp
 */
EmberApp.PaymentmanagerAPIAdapter = DS.RESTAdapter.extend({
	namespace: 'customer/apps/paymentmanager'
});
