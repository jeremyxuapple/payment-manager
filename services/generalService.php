<?php

namespace MiniBC\addons\paymentmanager\services;

use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\SingletonInterface;

class generalService implements SingletonInterface 
{
	/**
    * Export the result from the search
    * @param $header - the title line of the report
    * @param $outputFormat - needed columns of the report 
    */

    public function exportResults($rows)
    {
        $header = array('Order', 'Order Status', 'Customer Name', 'Customer Email', 'Card On File', 'Quantity', 'SKU', 'Product Name');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($rows as $row) {
             $searchRow = array(
                'order_id' => $row['order_id'],
                'status' => $row['status'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'cardOnFile' => $row['cardOnFile'],
                'quantity' => $row['quantity'],
                'sku' => $row['sku'],
                'name' => $row['name'],
            );
            fputcsv($output, $searchRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }


    /**
    * Grab the customer information base on orderId
    */

    public function getCustomerInformation($orderId, $productName)
    {   
        $customerStoreId = $this->customer->id;
        $query = "
            SELECT p.order_id , o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email,p.name, p.quantity, p.sku, ROUND(p.quantity*p.total_inc_tax, 2) AS total, pp.gateway_data
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products p
                    ON p.order_id = o.bc_id
                LEFT JOIN rc_customer_profiles cp
                    ON o.bc_customer_id = cp.store_customer_id
                LEFT JOIN rc_payment_profiles pp
                    ON cp.profile_id = pp.customer_profile_id
                WHERE o.customer_id = $customerStoreId
                AND o.bc_id = $orderId
                AND p.name = '$productName'
                ";

        $info = $this->db->queryFirst($query);

        $gateway_data = unserialize($info['gateway_data']);
        $last4 = $gateway_data['last4'];
        $expiry = $gateway_data['expiry'];

        unset($info['gateway_data']);
        $info['last4'] = $gateway_data['last4'];
        $info['expiry'] = $gateway_data['expiry'];

        return $info;
    }
    
}