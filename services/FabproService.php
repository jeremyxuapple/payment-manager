<?php

namespace MiniBC\addons\paymentmanager\services;

use MiniBC\core\entities\Store;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\interfaces\SingletonInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use MiniBC\addons\recurring\services\PaymentService;
use MiniBC\addons\recurring\services\PaymentProfileService;

class FabproService implements SingletonInterface
{
  private static $instance;
  /**
  * Fab Pro authorize the order with fraud check
  * @param orders: the orders need to be authorized
  * @param customerId: the customer id of the application
  * @param origin: the origin name of current store to differenitate stores
  */

  public function fabproAuth($orders, $customerId, $store, $origin)
  {
    $db = ConnectionManager::getInstance('mysql');
    $authorizeList = array();

    foreach ($orders as $order) {

        $orderId = $order['id'];
        $amount = $order['total'];

        $paymentProfileQuery = "SELECT rpp.id, rpp.gateway
                                FROM rc_payment_profiles rpp
                                JOIN bigbackup_bc_orders o
                                JOIN rc_customer_profiles rcp
                                    ON rcp.store_customer_id = o.bc_customer_id
                                    AND rcp.profile_id = rpp.customer_profile_id
                                WHERE o.bc_id = $orderId
                                AND o.customer_id = $customerId
                                AND rpp.customer_id = $customerId
                                AND rcp.customer_id =$customerId
                                ";

        $paymentData = $db->queryFirst($paymentProfileQuery);
        $orderResponse = array();
    try {
      $gateway = PaymentService::getInstance()->getGateway($paymentData['gateway'], $store);
      $profile = PaymentProfileService::getInstance()->getFromId($paymentData['id'], $store);
      // set gateway to authorize only mode
      $gateway->paymentTransactionType = 'authOnlyTransaction';
      // set metadata for the gateway
      $metadata = array('orderId' => $orderId);

      // create transaction
      $response = $gateway->createTokenizedPayment($amount, $profile, $metadata);

      $orderResponse['transactionId'] = $response->getTransactionId();
      $orderResponse['gateway'] = $paymentData['gateway'];
      $api = $store->getApiConnection();

      if (isset($orderResponse['cv_code'])) {
        $orderResponse['cv_code'] = $response->getTransactionData()['cv_code'];
      }

      if ($response->getStatus() == 'payment_success') {
        $orderResponse['payment_status'] = 'Fully Authorized';
        $orderResponse['avs_code'] = $response->getTransactionData()['avs_code'];

        switch ($_POST['company']) {
          case 'fabpro':
            // Update the order status to 'Awaiting Payment' for Fab Pro
            $api::updateResource('/orders/' . $orderId, (object)array('status_id' => 7));
          default:
            # code...
            break;
        }
      } else {
        $orderResponse['payment_status'] = 'Failed Authorization';
        $orderResponse['avs_code'] = 'E';
      }

      $orderResponse['payment_msg'] = $response->getMessage();

      $fraudCheck = $this->fraudCheck($customerId, $orderId, $api, $orderResponse, $origin);
      $orderResponse['fraud_check'] = $fraudCheck;

      $customer_info = $this->persistOrderResponse($orderResponse, $customerId, $orderId);
      $orderResponse = array_merge($orderResponse, $customer_info);

    } catch (\Exception $e) {
      // handle exception here
    }

    array_push($authorizeList, $orderResponse);

    } // End of the for loop

    return JsonResponse::create($authorizeList);
  }

  /**
  * Fab Pro capture orders
  */
  public function fabproCapture($orders, $customerId, $store)
  {
    $db = ConnectionManager::getInstance('mysql');
    $captureList = array();

    foreach ($orders as $order) {

      $orderId = $order['id'];

      $transactionId = $db->queryFirst(
        "SELECT transaction_id
        FROM pm_fp_orders
        WHERE order_id = $orderId
        AND customer_id = $customerId")
      ['transaction_id'];

      if ($transactionId == null) {
        $transactionId = $db->queryFirst(
          "SELECT payment_provider_id
          FROM bigbackup_bc_orders
          WHERE bc_id = $orderId
          AND customer_id = $customerId")
        ['payment_provider_id'];
      }

      // Perform the capture operation
      $gateway = PaymentService::getInstance()->getGateway('cybersource', $store);
      $metadata = array('orderId' => $orderId);
      $response = $gateway->captureAuthorizedPayment($order['total'], $transactionId, $metadata);

      if ($response->getStatus() == 'payment_success') {
        $api = $store->getApiConnection();
        $orderResponse['payment_status'] = 'Captured';
        switch ($_POST['company']) {
          case 'fabpro':
            // Update the order status to 'Awaiting Payment' for Fab Pro
            $api::updateResource('/orders/' . $orderId, (object)array('status_id' => 7));
          default:
            # code...
            break;
        }
      } else {
        $orderResponse['payment_status'] = 'Failed Capture';
      }

      $orderResponse['transactionId'] = $response->getTransactionId();
      $orderResponse['payment_msg'] = $response->getMessage();

      $customer_info = $this->persistOrderResponse($orderResponse, $customerId, $orderId);
      $orderResponse = array_merge($orderResponse, $customer_info);

      array_push($captureList, $orderResponse);
    }

    return JsonResponse::create($captureList);
  }


  /**
  * Export CSV report for the authorization and capture result
  */

  public function fabproExport($orders, $customerId)
  {
    $db = ConnectionManager::getInstance('mysql');
    $header = array('Status', 'Message', 'Signifyd Result', 'Order', 'Customer Name', 'Customer Email', 'Auth Amount');

    // send file header
    header("Content-Type: text/csv;charset=utf-8");
    header("Content-Disposition: attachment; filename=file.csv");
    header("Pragma: no-cache");
    header("Expires: 0");

    // write CSV to output stream
    $output = fopen("php://output", "w");
    // write header
    fputcsv($output, $header);

    foreach ($orders as $id) {
        $query = "
          SELECT fo.payment_status, fo.payment_msg, fo.fraud_status, o.bc_id AS order_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name,
          o.billing_email AS email, o.total_inc_tax AS total
          FROM bigbackup_bc_orders o
          LEFT JOIN pm_fp_orders fo
            ON fo.order_id = o.bc_id
            AND fo.customer_id = o.customer_id
          WHERE o.customer_id = $customerId
          AND o.bc_id = $id";

        $row = $db->queryFirst($query);
        fputcsv($output, $row); // here you can change delimiter/enclosure
    }

    fclose($output);
    exit;

  }

  /**
  * Persist the order transaction response result to the database
  * @param Array $orderResponse
  */

  public function persistOrderResponse($orderResponse, $customerId, $orderId)
  {
    $db = ConnectionManager::getInstance('mysql');
    extract($orderResponse);

    // Query if there is still a record already
    $query = sprintf("SELECT * FROM pm_fp_orders WHERE order_id = %s AND customer_id = %s", $orderId, $customerId);
    $existence = $db->queryFirst($query);

    if ($existence == null) {
      // Insert new row to the table
      $db->insert('pm_fp_orders', array(
        'order_id' => $orderId,
        'customer_id' => $customerId,
        'payment_status' => $payment_status,
        'fraud_status' => isset($fraud_check) ? $fraud_check : null,
        'transaction_id' => $transactionId,
        'payment_msg' => $payment_msg,
      ));

    } else {
      // Update the status and transaction Id

      $update = array( 'payment_status' => $payment_status );
      if (isset($fraud_check)) $update['fraud_status'] = $fraud_check;
      if ($transactionId != null) $update['transaction_id'] = $transactionId;

      $db->update('pm_fp_orders', $update,
        array('customer_id' => $customerId,
        'order_id' => $orderId)
      );
    }

    $customer_info = $db->queryFirst("
      SELECT bc_id AS order_id, CONCAT_WS(' ', billing_first_name, billing_last_name) AS customer_name, billing_email AS email, total_inc_tax AS total
        FROM bigbackup_bc_orders
        WHERE customer_id = $customerId
        AND bc_id = $orderId
    ");

    $orderResponse['order_id'] = $customer_info['order_id'];
    $orderResponse['customer_id'] = $customerId;
    $orderResponse['customer_name'] = $customer_info['customer_name'];
    $orderResponse['email'] = $customer_info['email'];
    $orderResponse['total'] = $customer_info['total'];

    return $orderResponse;
  }

  /**
  * Fraud check for transaction after authorization
  * @param $customerId: Int
  * @param $orderId: Int
  * @param $api: Api for the current Big Commerce store
  * @param $authResponse: CybersourcePaymentResponse
  * @param $origin: the origin of current store
  */
  public function fraudCheck($customerId, $orderId, $api, $authResponse, $origin)
  {
    // $orderId = 109;
    // $customerId = 20;
    // $bc_customer_id = 2;

    $db = ConnectionManager::getInstance('mysql');
    $order = $api::getResource('/orders/' . $orderId);

    $products = $api::getCollection('/orders/' . $orderId . '/products');
    $bc_customer_id = $order->customer_id;


    $shippingaddress = $api::getCollection('/orders/' . $orderId . '/shippingaddresses');
    // $shipments = $api::getCollection('/orders/' . $orderId . '/shipments');

    $products = $db->query(
      "SELECT bc_id, product_id, name, type, quantity, total_inc_tax, weight
        FROM bigbackup_bc_orders_products
        WHERE customer_id = $customerId AND order_id = $orderId"
    );

    $fraudCheckProducts = array();


    foreach ($products as $product) {
      $rawProduct = $api::getResource('/products/' . $product['product_id']);

      $p = array(
        'itemId' => $product['bc_id'],
        'itemName' => $product['name'],
        'itemIsDigital' => $product['type']=='digital' ? true : false,
        // 'itemCategory' => 'apparel',
        // 'itemSubCategory' => 'footwear',
        'itemUrl' => $origin . $rawProduct->custom_url,
        // 'itemImage' => 'http://mydomain.com/images/sparkly-sandals.jpeg',
        'itemQuantity' => $product['quantity'],
        'itemPrice' => $product['total_inc_tax'],
        'itemWeight' => $product['weight'],
      );

      array_push($fraudCheckProducts, $p);
    }

    // Discount Rules
    $discountCodes = array();
    if ($order->discount_amount != 0) {
      $discountRule['amount'] = $order->discount_amount;
      $discountRule['code'] = $order->cart_id;
      array_push($discountCodes, $discountRule);
    }

    $coupons = $db->query(
      "SELECT code, amount, type, discount
        FROM bigbackup_bc_orders_coupons
        WHERE customer_id = $customerId AND order_id = $orderId"
    );

    if ($coupons != null) {
      foreach ($coupons as $coupon) {
        $discountRule = array();
        if ($coupon['type'] == 1) {
          $type = 'percentage';
        } else {
          $type = 'amount';
        }
        $discountRule['code'] = $coupon['code'];
        $discountRule[$type] = $coupon['amount'];
        array_push($discountCodes, $discountRule);
      }
    }

    // Shipping Array
    $shipments = array();
    foreach ($shippingaddress as $shipping) {
      switch ($origin) {
        // case 'https://uk-shop.3dsystems.com':
        //   $shippingMethod = 'ground';
        //   break;
        case 'https://na-shop.3dsystems.com':
          // North American Store shipping method association
          switch ($shipping->shipping_method) {
            case 'Standard Overnight':
              $shippingMethod = 'overnight';
              break;
            case 'FedEx 2 Day':
              $shippingMethod = 'two_day';
              break;
            case 'International Priority':
              $shippingMethod = 'priority_international';
              break;
            case 'International Economy':
              $shippingMethod = 'international';
              break;
            case 'FedEx 2 Day Freight':
              $shippingMethod = 'freight';
              break;
            case 'FedEx 3 Day Freight':
              $shippingMethod = 'freight';
              break;
            case 'FedEx Ground':
              $shippingMethod = 'ground';
              break;
            case 'Ground Home Delivery':
              $shippingMethod = 'ground';
              break;
            case 'International Economy Freight':
              $shippingMethod = 'international';
              break;
            default:
              $shippingMethod = 'ground';
              break;
          }

        // case 'https://eu-shop.3dsystems.com':
        //   $shippingMethod = 'ground';
        //   break;
        default:
          $shippingMethod = 'ground';
          break;
      }

      $ship['shippingMethod'] = $shippingMethod;
      $ship['shippingPrice'] = $shipping->cost_inc_tax;
      $ship['shipper'] = 'FEDEX';
      array_push($shipments, $ship);
    }

    // Recipient
    $recipient = array(
      'fullName' => $shippingaddress[0]->first_name . ' ' . $shippingaddress[0]->last_name,
      'confirmationEmail' => $shippingaddress[0]->email,
      'confirmationPhone' => $shippingaddress[0]->phone,
      'organization' => $shippingaddress[0]->company,
      'deliveryAddress' => array(
        'streetAddress' => $shippingaddress[0]->street_1,
        'unit' => $shippingaddress[0]->street_2,
        'city' => $shippingaddress[0]->city,
        'provinceCode' => $shippingaddress[0]->state,
        'postalCode' => $shippingaddress[0]->zip,
        'countryCode' => $shippingaddress[0]->country_iso2)
    );

    $purchase = array(
      'orderSessionId' => $order->cart_id,
      'browserIpAddress' => $order->ip_address,
      'orderId' => $order->id,
      'createdAt' => date("c", strtotime($order->date_created)),
      'paymentGateway' => $order->payment_method,
      'paymentMethod' => 'credit_card',
      'transactionId' => $order->payment_provider_id,
      'currency' => $order->currency_code,
      'avsResponseCode' => $authResponse['avs_code'],
      'cvvResponseCode' => isset($authResponse['cv_code']) ? $authResponse['cv_code'] : 'P',
      'orderChannel' => 'WEB',
      // 'receivedBy' => 'John Doe',
      'totalPrice' => $order->total_inc_tax,
      'products' => $fraudCheckProducts,
      'discountCodes' => $discountCodes,
      'shipments' => $shipments,
    );

    $query = "SELECT pp.gateway_data
              FROM bigbackup_bc_orders o
              LEFT JOIN rc_customer_profiles cp
                  ON o.bc_customer_id = cp.store_customer_id
              LEFT JOIN rc_payment_profiles pp
                  ON cp.profile_id = pp.customer_profile_id
              WHERE o.customer_id = $customerId
              AND o.bc_id = $orderId";

    $paymentProfile = unserialize($db->queryFirst($query)['gateway_data']);

    $card = array(
      'cardHolderName' => $order->billing_address->first_name . ' ' . $order->billing_address->last_name,
      'bin' => isset($paymentProfile['bin']) ? $paymentProfile['bin'] : 411111,
      'last4' => $paymentProfile['last4'],
      'expiryMonth' => substr($paymentProfile['expiry'], 0, 2),
      'expiryYear' => substr($paymentProfile['expiry'], 3),
      'billingAddress' => array(
        'streetAddress' => $paymentProfile['address']['street_address'],
        'unit' => $order->billing_address->street_2,
        'city' => $paymentProfile['address']['city'],
        'provinceCode' => $paymentProfile['address']['state'],
        'postalCode' => $paymentProfile['address']['zip'],
        'countryCode' => $paymentProfile['address']['country'],
     )
    );

    // userAccount
    $aggregateInfo = $db->queryFirst(
      "SELECT SUM(o.total_inc_tax) AS totalAmount, COUNT(o.bc_id) AS orderCount, c.date_created,
      c.email, c.phone
        FROM bigbackup_bc_orders o
      LEFT JOIN bigbackup_bc_customers c
        ON o.bc_customer_id = c.bc_id
      WHERE o.bc_customer_id = $bc_customer_id
      AND o.customer_id = $customerId");

    $userAccount = array(
      'email' => $aggregateInfo['email'],
      // 'username' => 'bobbo',
      'phone' => $aggregateInfo['phone'],
      'createdDate' => date("c", strtotime($aggregateInfo['date_created'])),
      // 'accountNumber' => '54321',
      // 'lastOrderId' => '4321',
      'aggregateOrderCount' => $aggregateInfo['orderCount'],
      'aggregateOrderDollars' => $aggregateInfo['totalAmount'],
      // 'lastUpdateDate' => '2013-01-18T17:54:31-05:00',

    );

    $fraudRequest = array(
      'purchase' => $purchase,
      'recipient' => $recipient,
      'card' => $card,
      'userAccount' => $userAccount,
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.signifyd.com/v2/cases");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);

    $fraudRequest = json_encode($fraudRequest);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fraudRequest);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Basic Q1VoNzRwVWRBZDFrQ2xTMFh4SnBsNG1JMjo="
    ));

    $response = json_decode(curl_exec($ch));
    curl_close($ch);

    $investigationId = (string)$response->investigationId;
    // Set postpone for executing the next curl request for the time to create the case in Signifyd
    sleep(1);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://api.signifyd.com/v2/cases/' . $investigationId,
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/json",
          "Authorization: Basic Q1VoNzRwVWRBZDFrQ2xTMFh4SnBsNG1JMjo="
        )
    ));

    $result = json_decode(curl_exec($curl));
    curl_close($curl);

    $guaranteeDisposition = isset($result->guaranteeDisposition) ? $result->guaranteeDisposition : 'PENDING';

    return $guaranteeDisposition;

  }

  public static function getInstance()
  {
    if (is_null(self::$instance)) {
      self::$instance = new self();
    }

    return self::$instance;
  }
}
