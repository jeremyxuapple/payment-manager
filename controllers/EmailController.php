<?php
namespace MiniBC\addons\paymentmanager\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\providers\SMTPMailer;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;


class EmailController
{
	private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];
    }

    /**
    * The acutal function to send out emails
    */
    public function sendEmails()
    {
        $customer_id = $this->customer->id;
        $failedOrders = $_POST['failedOrders'];
        $templateId = $_POST['template']['id'];

        $template = $this->db->queryFirst("
            SELECT t.subject, t.body, t.bcc, p.header, p.footer
            FROM pm_email_templates t
            LEFT JOIN pm_email_parts p
                ON t.customer_id = p.customer_id
            WHERE t.id = $templateId
            ");

        $content = $template['header'] . $template['body'] . $template['footer'];

        $settings = $this->db->queryFirst("
            SELECT * FROM pm_email_settings WHERE customer_id = $customer_id
            ");

        // Build email base on template id, grab header and footer, loop through the failed email list
        foreach ($failedOrders as $order) {

            $templateVariables = array(
                '%%CustomerName%%'      => $order['customer_name'],
                '%%CustomerEmail%%'     => $order['billing_email'],
                '%%OrderId%%'           => $order['order_id'],
                '%%ProductSKU%%'        => isset($order['sku']) ? $order['sku'] : $order['product_sku'],
                '%%ProductName%%'       => isset($order['name']) ? $order['name'] : $order['product_name'],
                '%%PaymentTotal%%'      => isset($order['total']) ? $order['total'] : $order['auth_total'],
                '%%PaymentCardNumLastFour%%'  => $order['last4'],
                '%%PaymentErrorMessage%%'     => $order['message'],
            );

            // send email
            $result = Mail::send(
                $content,
                $templateVariables,
                function(Message &$message) use ($template, $settings, $order) {
                    //assign sender information

                    $recipientName = $order['customer_name'];

                    $message
                        ->to($order['billing_email'], $recipientName)
                        ->from($settings['outgoing_email'], $settings['company'])
                        ->subject($template['subject']);

                    if($template['bcc'] !== '' && $template['bcc'] !== null) {
                        $message->bcc($template['bcc'], $template['bcc']);
                    }
                  }
            );
        }

    }

    /**
    * Grab the email templates for the admininstrator to notify the failed transactions
    */

    public function getEmailTemplates()
    {
    	$customer_id = $this->customer->id;
    	$list = $this->db->query("SELECT * FROM pm_email_templates WHERE customer_id = $customer_id");
      $parts = $this->db->queryFirst("SELECT * FROM pm_email_parts WHERE customer_id = $customer_id");

        if ( isset($_GET['onlyNames']) ) {
            $names = $this->db->query("SELECT id, name AS label FROM pm_email_templates WHERE customer_id = $customer_id");

            return JsonResponse::create($names);
        }

				if ($parts == null) {
					$parts = array(
						'customer_id' => $customer_id,
						'footer' => 'This is the footer, please save for using',
						'header' => 'This is the header, please save for using'
					);
				}

        $response['parts'] = $parts;
        $response['list'] = $list;

    	return JsonResponse::create($response);
    }

    /**
    * Get a specific email template for editing or creating a new email template
    * @param - $id: the id of the email template or if it's 0 meaning to create a new template
    */

    public function getEmailTemplate($id)
    {
        $customer_id = $this->customer->id;
        // This is preparing for the create template route
        if ($id == 0) {
            $template = array(
                'creating' => true,
                'customer_id' => $customer_id,
                'name' => '',
                'subject' => '',
                'body' => '',
                'bcc' => '',
                'date_created' => '',
                'date_updated' => ''
            );
        } else {
        // This is the regular template return
            $template = $this->db->queryFirst("SELECT * FROM pm_email_templates WHERE customer_id = $customer_id AND id = $id");
        }

        return JsonResponse::create($template);
    }

    /**
    * Save the email template after it be edited
    * @param - $id: the id of the template
    */

    public function saveEmailTemplate($id)
    {
        $customer_id = $this->customer->id;
        $template = $_POST['template'];

        if ($id == 0) {
            $this->db->insert('pm_email_templates', $template);

        } else {

            $this->db->update("pm_email_templates", $template, array('customer_id' => $customer_id, 'id' => $id));

        }

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Save the Header and Footer of the email template
    */

    public function saveEmailParts()
    {
        $parts = $_POST['parts'];
        $customer_id = $this->customer->id;

				$verify_exist = $this->db->queryFirst("SELECT id FROM pm_email_parts WHERE customer_id = $customer_id")['id'];

				if ($verify_exist == null) {
					$this->db->insert('pm_email_parts', $parts);
				} else {
					$this->db->update("pm_email_parts", $parts, array('customer_id' => $customer_id));
				}

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Get the email settings such as send_from and company name, etc.
    */

    public function getEmailSettings()
    {
        $customer_id = $this->customer->id;
        $settings = $this->db->queryFirst("SELECT * FROM pm_email_settings WHERE customer_id = $customer_id");

				if ($settings == null) {
					$settings = array(
						'customer_id' => $customer_id,
						'company' => 'More Money',
						'outgoing_email' => 'More Money',
						'hostname' => 'More Money',
						'username' => 'More Money',
						'password' => 'More Money',
						'port' => 8888
					);
				}

        return JsonResponse::create($settings);
    }

    /**
    * Save the email settings such as send_from and company name, etc.
    */

    public function saveEmailSettings()
    {
        $customer_id = $this->customer->id;
        $settings = $_POST['settings'];

				$verify_exist = $this->db->queryFirst("SELECT id FROM pm_email_settings WHERE customer_id = $customer_id")['id'];

				if ($verify_exist == null){
					$this->db->insert('pm_email_settings', $settings);
				} else {
					$this->db->update("pm_email_settings", $settings, array('customer_id' => $customer_id));
				}

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Setup the SMTP testing
    */

    public function SMTPTesting()
    {
        $customer_id = $this->customer->id;

        $settings = $this->db->queryFirst("
            SELECT * FROM pm_email_settings where customer_id = $customer_id
            ");

        $config = array(
            'host' => $settings['hostname'],
            'backup_host' => '',
            'port' => $settings['port'],
            'username' => $settings['username'],
            'password' => $settings['password'],
            'enable_ssl' => false,
            'enable_tls' => false
        );

        $mailer = new SMTPMailer($config);

        $result = Mail::send('This is my email content.', array(), function(Message &$message) use ($settings) {

            $message
                ->to($settings['username'], 'SMTP Tester')
                ->from($settings['outgoing_email'], $settings['company'])
                ->subject('SMTP Testing');

        }, $mailer);


    }

}
