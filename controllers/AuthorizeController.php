<?php

namespace MiniBC\addons\paymentmanager\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\Log;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use MiniBC\addons\recurring\services\PaymentService;
use MiniBC\addons\recurring\services\PaymentProfileService;
use MiniBC\addons\paymentmanager\services\FabproService;

class AuthorizeController {
	private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
      $this->db = ConnectionManager::getInstance('mysql');
      $this->customer = Auth::getInstance()->getCustomer();
      $this->store = $this->customer->stores[0];
    }

		/**
		* Fab Pro Authroze Orders with Fraud Check
		*/

		public function fabproAuth()
		{
			$fabproService = FabproService::getInstance();
			$customer_id = $this->customer->id;
	    $orders = $_POST['orders'];
			$origin = $_POST['origin'];
			$response = $fabproService->fabproAuth($orders, $customer_id, $this->store, $origin);

			return $response;
		}

		/**
		* Fab Pro Capture Orders
		*/

		public function fabproCapture()
		{
			$fabproService = FabproService::getInstance();
			$orders = $_POST['orders'];
			$response = $fabproService->fabproCapture($orders, $this->customer->id, $this->store);

			return $response;
		}

    /**
    * Authorize Orders
    */

    public function authorizeOrders()
    {
        $customer_store_id = $this->customer->id;
        $authroizeList = array();
        $orders = $_POST['orders'];
        $orderId = $orders[0]['id'];
        $productName = $this->db->queryFirst("SELECT name FROM bigbackup_bc_orders_products WHERE order_id=$orderId AND customer_id=$customer_store_id")['name'];

        foreach ($orders as $order) {

            $orderId = $order['id'];
            $amount = $order['total'];

            $paymentProfileQuery = "SELECT rpp.id, rpp.gateway
                                    FROM rc_payment_profiles rpp
                                    JOIN bigbackup_bc_orders o
                                    JOIN rc_customer_profiles rcp
                                        ON rcp.store_customer_id = o.bc_customer_id
                                        AND rcp.profile_id = rpp.customer_profile_id
                                    WHERE o.bc_id = $orderId
                                    AND o.customer_id = $customer_store_id
                                    AND rpp.customer_id = $customer_store_id
                                    AND rcp.customer_id =$customer_store_id
                                    ";

            $paymentData = $this->db->queryFirst($paymentProfileQuery);
            $baseInfo = $this->getCustomerInformation($orderId, $productName);

            // print_r($baseInfo);
            // exit();

        try {

    			$gateway = PaymentService::getInstance()->getGateway($paymentData['gateway'], $this->store);

    			$profile = PaymentProfileService::getInstance()->getFromId($paymentData['id'], $this->store);

    			// set gateway to authorize only mode
    			$gateway->paymentTransactionType = 'authOnlyTransaction';

    			// set metadata for the gateway
    			$metadata = array('orderId' => $orderId);

    			// create transaction
    			$response = $gateway->createTokenizedPayment($amount, $profile, $metadata);

                $responseLog = get_object_vars($response);
                Log::addInfo('authorize response:', $responseLog);

                $baseInfo['transactionId'] = $response->getTransactionId();
                $baseInfo['gateway'] = $paymentData['gateway'];
								$api = $this->store->getApiConnection();

    			if ($response->getStatus() == 'payment_success') {
						$baseInfo['payment_status'] = 'Success';
						switch ($_POST['company']) {
							case 'gg':
								// Update the order status from 'Awaiting Payment' to 'Awaiting Fulfillment' for Gentle Gaint
								$api::updateResource('/orders/' . $orderId, (object)array('status_id' => 11));
								break;
							case 'fabpro':
								// Update the order status from 'Awaiting Payment' to 'Awaiting Payment' for Fab Pro
								$api::updateResource('/orders/' . $orderId, (object)array('status_id' => 7));
							default:
								# code...
								break;
						}

    			} else {
    				$baseInfo['payment_status'] = 'Failed';
						switch ($_POST['company']) {
							case 'gg':
								// Update the order status from 'Awaiting Payment' to 'Awaiting Fulfillment' for Gentle Gaint
								$api::updateResource('/orders/' . $orderId, (object)array('status_id' => 6));
								break;
							// case 'fabpro':
							// 	// Update the order status from 'Awaiting Payment' to 'Awaiting Payment' for Fab Pro
							// 	$api::updateResource('/orders/' . $orderId, (object)array('status_id' => 7));
							// default:
								# code...
								break;
						}
    			}
            $baseInfo['message'] = $response->getMessage();
    		} catch (\Exception $e) {
    			// handle exception here
    		}

          array_push($authroizeList, $baseInfo);

        } // End of the for loop

        return JsonResponse::create($authroizeList);
    }

    /**
    * Save the authorize response list into the database for later display and udpate
    */

    public function saveAuthList()
    {
        $customer_id = $this->customer->id;
        $listName = $_POST['listName'];
        $orders = $_POST['orders'];

        $authList = array(
            'name' => $listName,
            'customer_id' => $customer_id,
            'status' => 'New',
            'orders_containing' => sizeof($orders),
            'date_created' => time(),
            'date_updated' => time()
        );

        $authListId = $this->db->insert('pm_auth_list', $authList);

        foreach ($orders as $order) {

             $authItem = array(
                'pm_auth_list_id' => $authListId,
                'order_id' => $order['order_id'],
                'customer_id' => $customer_id,
                'bc_customer_id' => $order['bc_customer_id'],
                'payment_status' => $order['payment_status'] == 'Failed' ? 'Failed' : 'Authorized',
                'message' => $order['message'],
                'gateway' => $order['gateway'],
                'last4' => $order['last4'],
                'expiry' => $order['expiry'],
                'transactionId' => $order['transactionId'],
                'customer_name' => $order['customer_name'],
                'billing_email' => $order['billing_email'],
                'product_name' => $order['name'],
                'product_qty' => $order['quantity'],
                'product_sku' => $order['sku'],
                'auth_total' => $order['total'],
                'date_created' => time(),
                'date_updated' => time()
            );

            $this->db->insert('pm_auth_item', $authItem);

         }
        $response = array('status' => 'success');
        return JsonResponse::create($response);
    }


    /*
    * Pulling the authorize list which have been saved into the database
    */
    public function getAuthLists()
    {
        $customer_id = $this->customer->id;
        $authLists = $this->db->query("
            SELECT id, name, status, orders_containing, from_unixtime(date_created) AS date_created
            FROM pm_auth_list
            WHERE customer_id = $customer_id
            ORDER BY date_created DESC
            ");

        return JsonResponse::create($authLists);
    }

    /**
    * Pulling the detailed orders of a specific authorize list which have been saved into the database
    */
    public function getAuthEditLists($id)
    {
        $response['pm-authlists-edit'] = array();
        $customerStoreId = $this->customer->id;
        $list = $this->db->query("
            SELECT *, FORMAT(auth_total, 2) AS auth_total FROM pm_auth_item
            WHERE pm_auth_list_id = $id
            AND customer_id = $customerStoreId
            ");
        $response['pm-authlists-edit']['id'] = $id;
        $response['pm-authlists-edit']['orderLists'] = $list;

				if (isset($_GET['export'])) {
					$this->exportAuthReport($list);
				}

        return JsonResponse::create($response);
    }

    /**
    * Adjust the authorize total: just update whatever we have saved inside of the databse, but not doing the acutual authorization adjustment
    */
    public function adjustAuthItem()
    {
        $id = $_POST['id'];
        $adjustedTotal = $_POST['adjustedTotal'];

        $this->db->update('pm_auth_item', array('auth_total' => $adjustedTotal), array('id' => $id));

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Void the authorized transactions
    * @param - $id: the id of the order needed to be voided.
    */
    public function cancelAuthItem($id)
    {
        if ($id == null) {
            $id = $_POST['id'];
        }

        $orderInfo = $this->db->queryFirst("
            SELECT gateway, transactionId FROM pm_auth_item WHERE id = $id
            ");

        try {
                $gateway = PaymentService::getInstance()->getGateway($orderInfo['gateway'], $this->store);

                $response = $gateway->voidAuthorizedPayment($orderInfo['transactionId']);

                if ($response->getStatus() == 'payment_success') {

                    $this->db->update('pm_auth_item',
                        array(
                            'payment_status' => 'Void',
                            'message' => $response->getMessage(),
                            'auth_total' => 0),
                        array( 'id' => $id )
                        );
                    if (isset($_POST['id'])) {
                        // When the function executed as an independent endponit
                        $res = array();
                        $res['success'] = true;
                        return JsonResponse::create($res);
                    }
                }


            } catch (\Exception $e) {
                 print_r($e);
                 exit();
            }
    }

    /**
    * Void all the orders passed in
    */
    public function cancelAllItems()
    {
        $orders = $_POST['orders'];

        foreach ($orders as $order) {
            $this->cancelAuthItem($order['id']);
        }

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Capture the authorized orders
    */
    public function captureOrders()
    {
        $orders = $_POST['orders'];

        foreach ($orders as $order) {
            try {
                $gateway = PaymentService::getInstance()->getGateway($order['gateway'], $this->store);
                $metadata = array('orderId' => $order['order_id']);
                $response = $gateway->captureAuthorizedPayment($order['auth_total'], $order['transactionId'], $metadata);

                if ($response->getStatus() == 'payment_success') {
                  $this->db->update('pm_auth_item',
                      array(
                          'payment_status' => 'Captured',
                          'message' => $response->getMessage(),
                          'transactionId' => $response->getTransactionId()
                          ),
                      array( 'id' => $order['id'] )
                      );

									$api = $this->store->getApiConnection();

									switch ($_POST['company']) {
										case 'gg':
											// Update the order status from 'Awaiting Payment' to 'Shipped' for Gentle Gaint
											$api::updateResource('/orders/' . $order['order_id'], (object)array('status_id' => 2));
											break;
										case 'fabpro':
											// Update the order status from 'Awaiting Payment' to 'Completed' for Fab Pro
											$api::updateResource('/orders/' . $order['order_id'], (object)array('status_id' => 10));
										default:
											# code...
											break;
										}

                } else {
                  $this->db->update('pm_auth_item',
                      array(
                          'payment_status' => 'Failed',
                          'message' => $response->getMessage()
                          ),
                      array( 'id' => $order['id'] )
                      );
                }

            } catch (\Exception $e) {

            }
        }

        $this->db->update('pm_auth_list', array('status' => 'Completed'), array( 'id' => $orders[0]['pm_auth_list_id'] ));

        $resp['success'] = true;
        return JsonResponse::create($resp);
    }


    /**
    * get the customer information base on orderId
    */

    public function getCustomerInformation($orderId, $productName)
    {
        $customerStoreId = $this->customer->id;
        $query = "
            SELECT p.order_id , o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email,p.name, p.quantity, p.sku, FORMAT(o.total_inc_tax, 2) AS total, pp.gateway_data
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products p
                    ON p.order_id = o.bc_id
                LEFT JOIN rc_customer_profiles cp
                    ON o.bc_customer_id = cp.store_customer_id
                LEFT JOIN rc_payment_profiles pp
                    ON cp.profile_id = pp.customer_profile_id
                WHERE o.customer_id = $customerStoreId
                AND o.bc_id = $orderId
                AND p.name = '$productName'
                ";

        $info = $this->db->queryFirst($query);

        $gateway_data = unserialize($info['gateway_data']);
        $last4 = $gateway_data['last4'];
        $expiry = $gateway_data['expiry'];

        unset($info['gateway_data']);
        $info['last4'] = $gateway_data['last4'];
        $info['expiry'] = $gateway_data['expiry'];

        return $info;
    }

    /**
    * Export authorization response list report
    */

    public function exportAuthReport($orders)
    {
        $header = array( 'Order', 'Status', 'Message', 'Customer Name', 'Customer Email', 'Authorized Amount');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=authResult.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($orders as $row) {
            $authorizeRow = array(
							  'order_id' => $row['order_id'],
                'payment_status' => $row['payment_status'],
                'message' => $row['message'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'total' => $row['auth_total']
            );
            fputcsv($output, $authorizeRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

    /**
    * Export Capture response list report
    */

    public function exportCaptureReport()
    {
        $orders = $_GET['orders'];

        $header = array('Status', 'Transaction ID', 'Failed Message', 'Order ID', 'Customer Name', 'Customer Email', 'Charge Amount');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($orders as $row) {

            $capturedRow = array(
                'payment_status' => $row['payment_status'],
                'transactionId' => $row['transactionId'],
                'message' => $row['message'],
                'order_id' => $row['order_id'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'auth_total' => $row['auth_total'],
            );
            fputcsv($output, $capturedRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

		public function guaranteesWebhook()
		{
			$webhook = file_get_contents("php://input");
			$data = json_decode($webhook, true);
			$customer_id = $this->customer->id;

			$orderId = $data['orderId'];
			if (isset($data['guaranteeDisposition']))
			{
				$this->db->update('pm_fp_orders',
					array('fraud_status' => $data['guaranteeDisposition']),
					array('customer_id' => $customer_id,
								'order_id' => $orderId));
			}

		}
}
