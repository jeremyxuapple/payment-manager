<?php
namespace MiniBC\addons\paymentmanager\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\paymentmanager\services\EmailService;
use MiniBC\addons\paymentmanager\services\FabproService;

class SearchController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
      $this->db = ConnectionManager::getInstance('mysql');
      $this->customer = Auth::getInstance()->getCustomer();
      $this->store = $this->customer->stores[0];
    }

    /**
    * Search order
    * @return JsonResponse|Response
    */

    public function searchOrder()
    {
        $customer_store_id = $this->customer->id;

        if (isset($_GET['orderID'])) {
            // Single Order Search
            $orderID = $_GET['orderID'];
            $search_query = "
                SELECT o.bc_id AS order_id, o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email, op.quantity, op.sku, op.name
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products op
                ON op.order_id = o.bc_id
                WHERE o.customer_id = $customer_store_id
                AND op.customer_id = $customer_store_id
                AND o.bc_id = $orderID";

            $order = $this->db->queryFirst($search_query);

            $onFile = $this->verifyCardOnFile($customer_store_id, $order['bc_customer_id']);
            $order['cardOnFile'] = $onFile ? 'YES' : 'NO';

            return JsonResponse::create(array($order));

        } else {
            // Complex search
            if (!isset($_GET['productName'])) {
                $res['msg']='The product name must be provided.';
                return JsonResponse::create($res, JsonResponse::HTTP_BAD_REQUEST);
            } else {
                $product_name = $_GET['productName'];
                $start = strpos($product_name, 'SKU: ');
                $sku = substr($product_name, $start + 5);
            }

            // $product_name = '[Sample] Orbit Terrarium - Small';
              $search_query = "
                  SELECT p.order_id , o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email, p.quantity, p.sku, p.name
                  FROM bigbackup_bc_orders o
                  LEFT JOIN bigbackup_bc_orders_products p
                  ON p.order_id = o.bc_id
                  WHERE o.customer_id = $customer_store_id
                  AND p.customer_id = $customer_store_id
                  AND p.sku = '$sku'
                  AND o.status_id NOT IN (0, 5)
                  ";

            $start_id = isset($_GET['startID']) ? $_GET['startID'] : false;
            $end_id = isset($_GET['endID']) ? $_GET['endID'] : false;
            $status = isset($_GET['orderStatus']) ? $_GET['orderStatus']['label'] : false;
            $type = isset($_GET['orderType']) ? $_GET['orderType']['label'] : false;

            if ($start_id) $search_query .= " AND o.bc_id >= $start_id";
            if ($end_id) $search_query .= " AND o.bc_id <= $end_id";
            if ($status) $search_query .= " AND o.status = '$status'";

            if (isset($_GET['orderType'])) {
                switch ($_GET['orderType']['id']) {
                    case 0:
                        $search_query .= " AND p.quantity = 1";
                        break;
                    case 1:
                        $search_query .= " AND p.quantity > 1";
                        break;
                    default:
                        break;
                }
            }
        }

        $search_query .= " ORDER BY o.bc_id DESC";

        $orders = $this->db->query($search_query);

        foreach ($orders as $order) {
            array_splice($orders, 0, 1);
            $onFile = $this->verifyCardOnFile($customer_store_id, $order['bc_customer_id']);
            $order['cardOnFile'] = $onFile ? 'YES' : 'NO';
            array_push($orders, $order);
        }

        // For export CSV report
        if (isset($_GET['exportCSV'])) {
            $this->exportResults($orders);
        }

        return JsonResponse::create($orders);
    }

    /**
    * Search order
    * @return JsonResponse|Response
    */

    public function searchOrderFabpro()
    {
      $customer_store_id = $this->customer->id;

      if (isset($_GET['orderID'])) {
          // Single Order Search
          $orderID = $_GET['orderID'];
          $search_query = "
              SELECT o.bc_id AS order_id, o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email
              FROM bigbackup_bc_orders o
              WHERE o.customer_id = $customer_store_id
              AND o.bc_id = $orderID";

          $order = $this->db->queryFirst($search_query);

          $onFile = $this->verifyCardOnFile($customer_store_id, $order['bc_customer_id']);
          $order['cardOnFile'] = $onFile ? 'YES' : 'NO';

          return JsonResponse::create(array($order));

      } else {
        $search_query = "
            SELECT o.bc_id as order_id, o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email
            FROM bigbackup_bc_orders o
            WHERE o.customer_id = $customer_store_id
            AND o.status_id NOT IN (0, 5)
            ";

        $start_id = isset($_GET['startID']) ? $_GET['startID'] : false;
        $end_id = isset($_GET['endID']) ? $_GET['endID'] : false;
        $status = isset($_GET['orderStatus']) ? $_GET['orderStatus']['label'] : false;

        if ($start_id) $search_query .= " AND o.bc_id >= $start_id";
        if ($end_id) $search_query .= " AND o.bc_id <= $end_id";
        if ($status) $search_query .= " AND o.status = '$status'";
      }

      $search_query .= " ORDER BY o.bc_id DESC";

      $orders = $this->db->query($search_query);

      foreach ($orders as $order) {
          array_splice($orders, 0, 1);
          $onFile = $this->verifyCardOnFile($customer_store_id, $order['bc_customer_id']);
          $order['cardOnFile'] = $onFile ? 'YES' : 'NO';
          array_push($orders, $order);
      }

      // For export CSV report
      if (isset($_GET['exportCSV'])) {
        $this->exportResultsFabPro($orders);
      }

      return JsonResponse::create($orders);
    }

    /**
    * Pulling all the orders for Fabpro
    */


    // Pending pre-order: 8
    // Awaiting payment: 7
    public function ordersFabpro()
    {
      $customer_id = $this->customer->id;
      $search_query = "
        SELECT o.bc_id AS order_id, SUBSTR(o.date_created, 6, 11) AS date, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name,
        o.billing_email AS email, o.total_inc_tax AS total, o.status_id, o.status, fo.payment_status, fo.fraud_status
        FROM bigbackup_bc_orders o
        LEFT JOIN pm_fp_orders fo
          ON fo.order_id = o.bc_id
          AND fo.customer_id = o.customer_id
        WHERE o.customer_id = $customer_id
        AND o.status_id != 0
        ORDER BY order_id DESC";

      $orders = $this->db->query($search_query);

      foreach ($orders as &$order) {

        switch ($order['status_id']) {
          case 7:
            $order['type'] = 'In-Stock';
            break;
          case 8:
            $order['type'] = 'Pre-Order';
            break;
          default:
            $order['type'] = $order['status'];
            break;
        }

      }

      return JsonResponse::create($orders);
    }

    /**
    * Charge confirm order
    * @param
    * @return JsonResponse|Response
    */

    public function authorizeConfirm()
    {
        $orders = $_GET['orders'];
        // $productName = $_GET['productName'];

        $customer_store_id = $this->customer->id;
        $selected_orders = array();

        foreach($orders as $id) {

            $selected_orders_query = "
                SELECT o.bc_id AS id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name,
                    pp.gateway_data, FORMAT(o.total_inc_tax, 2) AS total
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products p
                    ON o.customer_id = p.customer_id
                    AND o.bc_id = p.order_id
                LEFT JOIN rc_customer_profiles cp
                    ON o.bc_customer_id = cp.store_customer_id
                LEFT JOIN rc_payment_profiles pp
                    ON cp.profile_id = pp.customer_profile_id
                WHERE o.bc_id = $id
                AND o.customer_id = $customer_store_id
            ";

            $order = $this->db->queryFirst($selected_orders_query);

            $gateway_data = unserialize($order['gateway_data']);
            $last4 = $gateway_data['last4'];
            $expiry = $gateway_data['expiry'];

            unset($order['gateway_data']);
            $order['last4'] = $gateway_data['last4'];
            $order['expiry'] = $gateway_data['expiry'];

            array_push($selected_orders, $order);
        }

        return JsonResponse::create($selected_orders);
    }

    /**
    * Export the result from the search
    *
    */

    public function exportResultsFabPro($rows)
    {
        $header = array('Order', 'Order Status', 'Customer Name', 'Customer Email', 'Card On File');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($rows as $row) {
             $searchRow = array(
                'order_id' => $row['order_id'],
                'status' => $row['status'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'cardOnFile' => $row['cardOnFile'],
            );
            fputcsv($output, $searchRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }



    /**
    * Export the result from the search
    *
    */

    public function exportResults($rows)
    {
        $header = array('Order', 'Order Status', 'Customer Name', 'Customer Email', 'Card On File', 'Quantity', 'SKU', 'Product Name');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($rows as $row) {
             $searchRow = array(
                'order_id' => $row['order_id'],
                'status' => $row['status'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'cardOnFile' => $row['cardOnFile'],
                'quantity' => $row['quantity'],
                'sku' => $row['sku'],
                'name' => $row['name'],
            );
            fputcsv($output, $searchRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

    /**
    * Verify if the card in on the file
    */
    public function verifyCardOnFile($customerStoreID, $customerId)
    {
        $query = "
                SELECT rpp.gateway_data
                FROM rc_customer_profiles rcp
                LEFT JOIN rc_payment_profiles rpp
                    ON rcp.profile_id = rpp.customer_profile_id
                    AND rcp.customer_id = rpp.customer_id
                WHERE rcp.customer_id = $customerStoreID
                AND rcp.store_customer_id = $customerId
                ";

        $profile = $this->db->queryFirst($query);

        return $profile['gateway_data'] != null ? true : false;
    }

    /**
    * Build proudct name list for options
    */
    public function searchProductNames()
    {
        $customer_id = $this->customer->id;
        $productNameQuery = "SELECT bc_id AS id, name AS label, sku FROM bigbackup_bc_products WHERE customer_id = $customer_id";

        $productNames = $this->db->query($productNameQuery);
        return JsonResponse::create($productNames);

    }


    /**
    * Export CSV report for the authorization and capture result
    */

    public function fabproExport()
    {
      $orders = $_GET['orders'];
      $fabproService = FabproService::getInstance();
      $fabproService->fabproExport($orders, $this->customer->id);
    }

    /**
    * Filter Orders base on the prodcut name
    */
    public function fabproFilterProductName($value='')
    {
      $customer_id = $this->customer->id;
      $name = $_GET['productName'];
      $query = "SELECT o.bc_id
                FROM bigbackup_bc_orders o
                LEFT JOIN bigbackup_bc_orders_products op ON op.order_id = o.bc_id
                AND op.customer_id = o.customer_id
                WHERE op.customer_id = $customer_id
                AND op.name = '$name'
                GROUP BY o.bc_id";

      $orders = $this->db->query($query);
      $ids = array();
      foreach ($orders as $order) {
        array_push($ids, $order['bc_id']);
      }
      return JsonResponse::create($ids);

    }

}
